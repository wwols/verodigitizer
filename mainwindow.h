#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <string>
#include <QMainWindow>
#include <Digitizer.h>
#include "EventAnalyzer.h"
//#include <pulseshapemanager.h>
#include "udp_server.h"
#include <json/json.h>
#include <QtWidgets>
#include <fstream>
#include <iostream>
#include <QtCharts/QLineSeries>
#include "CAENDigitizer.h"
#include <QtCharts/QLogValueAxis>
#include <QtCharts/QValueAxis>

QT_CHARTS_USE_NAMESPACE

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(Json::Value root, udp_server::udp_server* server, char* configFileName, QWidget *parent = 0);
    ~MainWindow();


//std::ifstream::pos_type MainWindow::filesize(const char* filename);

public slots:
    void updateDigitizerInfo();
    void updateRunStatus();
    void updateDisplay(int length, double* array);
    void startDAQ();
    void stopDAQ();

private slots:
    void on_plotPushButton_clicked();

    void on_dscfdStartLineEdit_editingFinished();

    void on_filterLenghtLineEdit_editingFinished();

    void on_saveSettingsPushButton_clicked();

    //void on_rebinLineEdit_editingFinished();

    void on_rebinLineEdit_editingFinished();

    void on_baseLineLineEdit_editingFinished();

    void on_integralLengthLineEdit_editingFinished();

    void on_dcfdStopLineEdit_editingFinished();

    void on_dcfdFractionLineEdit_editingFinished();

    void on_dcfdThresholdLineEdit_editingFinished();

    void on_dcfdDelayLineEdit_editingFinished();

    void on_dcfdFilterLengthLineEdit_editingFinished();

    void on_integralCutCheckBox_toggled(bool checked);

    void on_integralMinThrLineEdit_editingFinished();

    void on_baseLineSdCutOnCheckBox_toggled(bool checked);

    void on_baseLineSdThresholdLineEdit_editingFinished();

    void on_dcfdCutOnCheckBox_toggled(bool checked);

    void on_zcNumberCutOnCheckBox_toggled(bool checked);

    void on_pisRangeLineEdit_editingFinished();

    //void on_triggerThresholdSpinBox_valueChanged(int arg1);

    void on_recordLengthLineEdit_editingFinished();

    void on_adcChannelSpinBox_valueChanged(int arg1);

    //void on_dcOffsetSpinBox_valueChanged(unsigned int arg1);

    void on_postTriggerSpinBox_valueChanged(int arg1);

    void saveSettingsToFile();

    void loadSettingsFromFile();

    void on_loadSettingsPushButton_clicked();

    void refreshUI();

    void on_resetButton_clicked();

    void on_psdCutOnCheckBox_toggled(bool checked);

    void on_psdMinLineEdit_editingFinished();

    void on_psdMaxLineEdit_editingFinished();

    void on_tergetZeroLineEdit_editingFinished();

    void on_aLineEdit_editingFinished();

    void on_bLineEdit_editingFinished();

    void on_saveDecayCurves();

    //void on_triggerThresholdLineEdit_editingFinished();

    void on_triggerThresholdLineEdit_textChanged(const QString &arg1);

    void on_dcOffsetSpinBox_valueChanged(const QString &arg1);

    void on_pulsePolarityComboBox_currentIndexChanged(int index);

    void on_triggerPolarityComboBox_currentIndexChanged(int index);

    void on_rejectClippedCheckBox_toggled(bool checked);

    void on_pushButton_clicked();


    void on_removePushButton_clicked();

    void on_psTableWidget_cellChanged(int row, int column);

    void setLogYaxis();

private:
    Ui::MainWindow *ui;
    Digitizer *digitizer;
    EventAnalyzer *analyzer;
    QStateMachine machine;
    QState *off;
    QState *on;
    QTimer *statusTimer;
    QLineSeries *series;
    QChart *chart;
    double* displayYdata;
    unsigned int displayYdataLength;
    Json::Value m_root;
    udp_server::udp_server* m_server;
    char* m_configFileName;
    std::string versionName;
    Json::Value pulseShapeDescriptions_;
};

#endif // MAINWINDOW_H
