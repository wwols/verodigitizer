#include "dcfd.h"
#include<vector>
#include<iostream>
using namespace std;
//Digital constant fraction discriminator C++ class
//August 2017
/*
class Dcfd{
 private:
  double * y;
  int maxPosition = startSample;
  int minPosition = startSample;
  int zeroPosition = startSample;
  int overThreshold = 0;
  int arrayLength;
  double preciseZero = 0;
  double thr = 0;

public:
  Dcfd(Int_t L, Int_t D, Double_t F, Double_t BL, unsigned short array[], Int_t startSample, Int_t stopSample, Double_t threshold);
    //N - length of the input array, L-length of moving avarage [samples], D-lenght of delay [samples], F-fraction of delay signal [0.-1.], BL-base line, array-input array   
  ~Dcfd();
  void processSignal(Double_t bl);
  Double_t getZeroCrossing(void);
  Double_t* getOutputSignal(void);
};
*/
//unsigned short * pArray
Dcfd::Dcfd(unsigned int pL, unsigned int pD, double pF, double * pArray, unsigned int pStartSample,
           unsigned int pStopSample, double pThreshold): L(pL), D(pD), F(pF), array(pArray),
           startSample(pStartSample), stopSample(pStopSample), threshold(pThreshold){
  arrayLength = stopSample-startSample;
  y = new double[arrayLength];
}

Dcfd::~Dcfd(void) {
  delete[] y;
}
  
void Dcfd::processSignal(double bl){
  for (unsigned int i = 0; i<arrayLength; i++){
    y[i]=0;
    double t0;
    double t1;
    for(unsigned int j = 0; j<L; j++){
      //cout<<startSample+i-j<<" "<<startSample+i-j-D<<endl;
      if((startSample+i-j)<=0) t0 = 0; else t0 = (double)array[startSample+i-j];
      if((startSample+i-j-D)<=0) t1 = 0; else t1 = (double)array[startSample+i-j-D];
      y[i] += (F*(t0-bl)-(t1-bl))/L;
    }
  }
  bl = 0;
  int dcfdBaseLineLength = 3; //<----- HARD CODED!                                        !!!!!!!!!!!!  Will crash with unproper pL !!!!!!!!!!!!!!!!!
  for(int i = 0; i < dcfdBaseLineLength; i++) bl+= y[i];
  bl/=dcfdBaseLineLength;
  for (unsigned int i = 0; i<arrayLength; i++) y[i]-=bl;
}
  
double Dcfd::getZeroCrossing(){
  overThreshold = 0;
  //cout<<"array length "<<arrayLength<<endl;
  for (unsigned int i = 0; i<arrayLength; i++) {
    //    cout<<i<<endl;
    //cout<<"thrs: "<<threshold<<endl;
    if(threshold>0){
      if(y[i]>(threshold)) overThreshold = 1;
      if(overThreshold!=0)
        if(y[i]<0) {
          zeroPosition=i;
          break;
        }
    }
    else{
      if(threshold<0){
        if(y[i]<(threshold)) overThreshold = 1;
        if(overThreshold!=0)
          if(y[i]>0) {
            zeroPosition=i;
            break;
          }
      }
    }
    if(i==arrayLength-1) return -1;
  }
  double preciseZero = ((zeroPosition-1)*y[zeroPosition]-zeroPosition*y[zeroPosition-1])/(y[zeroPosition]-y[zeroPosition-1]);
  return preciseZero;
}

std::vector<double> Dcfd::getZeroCrossingVec(){
  std::vector<double> zcVector;
  double preciseZero;
  overThreshold = 0;
  for (int i = 0; i<arrayLength; i++) {
      if(threshold>0){
          if(y[i]>(threshold)) overThreshold = 1;
          if(overThreshold!=0)
            if(y[i]<0) {
                zeroPosition=i;
                preciseZero = ((zeroPosition-1)*y[zeroPosition]-zeroPosition*y[zeroPosition-1])/(y[zeroPosition]-y[zeroPosition-1]);
                zcVector.push_back(preciseZero);
                overThreshold = 0;
              }
        }
      else{
          if(threshold<0){
              if(y[i]<(threshold)) overThreshold = 1;
              if(overThreshold!=0)
                if(y[i]>0) {
                    zeroPosition=i;
                    preciseZero = ((zeroPosition-1)*y[zeroPosition]-zeroPosition*y[zeroPosition-1])/(y[zeroPosition]-y[zeroPosition-1]);
                    zcVector.push_back(preciseZero);
                    overThreshold = 0;
                  }
            }
        }
    }

    //if(y[i]<(-1.*threshold)) overThreshold = 1;
    //if(overThreshold == 1)
      //if(y[i]>0) {
        //zeroPosition=i;
        //cout<<"zeroPosition: "<<zeroPosition;
        //preciseZero = ((zeroPosition-1)*y[zeroPosition]-zeroPosition*y[zeroPosition-1])/(y[zeroPosition]-y[zeroPosition-1]);
	//cout<<" preciseZero: "<<preciseZero<<endl;
	//zcVector.push_back(preciseZero);
	//overThreshold = 0;
    //  }
  //}
  return zcVector;
}

double* Dcfd::getOutputSignal(){
    return y;
  }
  
