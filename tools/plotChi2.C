#include "Riostream.h"
#include <stdio.h>
#include <stdlib.h>


void plotChi2() {
//  Plot Chi^2 from an root file and print on screen 
//Author: W. W. Wolszczak 2017
  //Pulse shape discrimiantion settings
  Double_t psdAlphaCut = 0.0050;
  Double_t alphaEnergyCut = 25e3;
  Double_t chi2RangeMax = 0.05; //0.01 for scintillation
  Double_t chi2RangeMin = -0.01;
  Double_t energyRange = 600e3; //300e3

  TString fileName = "OutputFileName.root"; //"CeBr3_2inch_withCalib_pulser_newBase.root"; //"LaBr3CeSr_withCalib_pulser_newBase.root";
  cout<<"Processing "<<fileName<<" file."<<endl;
   TString dir = gSystem->UnixPathName(__FILE__);
   dir.ReplaceAll("readTTree.C","");
   dir.ReplaceAll("/./","/");
   TFile *f = new TFile(fileName);//LaBr3CeSr_intr_act.root");
   TTree *tree = (TTree*)f->Get("tree");
   const int MAXEVENTS = 30000;
   unsigned short Waveform[MAXEVENTS];//, Ee[MAXEVENTS], Eg[MAXEVENTS];
   Double_t Energy, psd;
   Int_t chTemperature;
   Int_t Nsamples;//, Ne, Ng;
   Double_t digitizerTime, pcTime, baseLine, chi2;
   tree->SetBranchAddress("Waveform",Waveform);
   tree->SetBranchAddress("Energy",&Energy);
   tree->SetBranchAddress("psd",&psd);
   tree->SetBranchAddress("NSamples",&Nsamples);
   tree->SetBranchAddress("digitizerTime",&digitizerTime);
   tree->SetBranchAddress("pcTime",&pcTime);
   tree->SetBranchAddress("baseLine",&baseLine);
   tree->SetBranchAddress("chi2",&chi2);
   
   TH2F *hChi2   = new TH2F("hChi2","Chi2 vs Energy",1024,0,energyRange,1024,chi2RangeMin,chi2RangeMax);
   TH1F *hb   = new TH1F("hb","Beta-type events energy spectrum",1024,0,energyRange);
   TH1F *ha   = new TH1F("ha","Energy spectrum",1024,0,energyRange);
   TH1F *hTotal   = new TH1F("hTotal","Energy spectrum",1024,0,energyRange);
   Int_t nentries = (Int_t)tree->GetEntries();

   
   //Double_t x[nPoints], y[nPoints], t[nPoints];
   
   

   //gr->Draw("");
   cout<<"Events to analyze: "<<nentries<<endl;
   //TGraph *gr = new TGraph(200,x,y);
   //nentries = 10000;
   for(int entry; entry<nentries; entry++){
     int clipped = 0;
     
     tree->GetEntry(entry);
     //     if(pcTime<11500) continue;

     //     for (int i = 0; i<Nsamples; i++){
     //if(Waveform[i]==0) clipped = 1;
     //}
     //if(clipped == 1) cout<<"Clipped! Energy = "<<Energy<<endl;
     //LaBr3CeSr 0.056 PSD
     //CeBr3 2" 0.0539
     //LaBr3 1" J8* 0.697
     //if((psd<psdAlphaCut)&(Energy>alphaEnergyCut)) ha->Fill(Energy); else hb->Fill(Energy);
     hChi2->Fill(Energy,chi2);
     //if(psd<0.04) hTotal->Fill(Energy);
     //if(pcTime>600) break;
   }

   //hPSD->Draw("COL");

   c1 = new TCanvas("c1");
   c1->SetLogy();
   hb->Draw("");
   ha->SetLineColor(3);
   ha->Draw("SAME");
   hTotal->SetLineColor(1);
   hTotal->Draw("SAME");

   c2 = new TCanvas("c2");
   c2->SetLogz();
   hChi2->Draw("COL");
}
