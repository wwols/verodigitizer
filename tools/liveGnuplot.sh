#set logscale y
set ylabel "base line"
set y2label "count rate"
set y2tics
set ytics nomirror
plot "countRate.txt" using 1:2 with lines axes x1y1, "countRate.txt" using 1:3 with lines axes x1y2
pause 1
reread