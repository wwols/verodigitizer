#ifndef PULSESHAPEMANAGER_H
#define PULSESHAPEMANAGER_H

#include <QThread>
#include <vector>
#include <PulseShape.h>
#include <EnergyCalibrator.h>
class PulseShapeManager : public QThread {
  Q_OBJECT
public:
  PulseShapeManager(EnergyCalibrator e);
  void addWaveform(int particleIndex, double baseLine, double integral, double zeroCrossingTime);
  void addPulseShape(int polarity, double* waveform, long int waveformLength,int particleIndex,int nbIntegralBins,double integralMin,double integralMax, double targetZeroPosition);
  double* getPulseShape(int psIndex, int integralIndex, char* option = (char*) "non");
  int getPulseShapesNumber() {return pulseShapeVector.size();}
  int getIntegralBinsNumber(int index) {return pulseShapeVector[index]->getNbIntegralBins();}
  double getBinWidth(int index) {return (pulseShapeVector[index]->getImax() - pulseShapeVector[index]->getImin())/pulseShapeVector[index]->getNbIntegralBins();}
  double getImin(int index) {return pulseShapeVector[index]->getImin();}
  double getImax(int index) {return pulseShapeVector[index]->getImax();}
  void resetCounters(); //reset counters of all PulseShapes
  void clearBuffers(); //clears all bufffers in all PulseShapes
  void reset(); //resets counters and clear all buffers in all PulseShapes
  void clear(); //deletes pulseShapeVector
  int saveAscii(char * fileName);
private:
  std::vector<PulseShape*> pulseShapeVector;
  EnergyCalibrator e;
};

#endif // PULSESHAPEMANAGER_H
