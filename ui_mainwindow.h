/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCharts/QChartView>
#include <QtCharts/chartsnamespace.h>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionLoead_settings;
    QAction *actionSave_settings;
    QAction *actionSave_decay_curves;
    QWidget *centralwidget;
    QHBoxLayout *horizontalLayout_3;
    QSplitter *splitter;
    QtCharts::QChartView *chartview;
    QTabWidget *tabWidget;
    QWidget *tab;
    QGridLayout *gridLayout_4;
    QGroupBox *groupBox_9;
    QGridLayout *gridLayout_6;
    QLabel *label_62;
    QLabel *amcLabel;
    QLabel *modelLabel;
    QLabel *label_44;
    QLabel *label_40;
    QLabel *label_56;
    QLabel *label_42;
    QLabel *formFactorLabel;
    QLabel *serialNumberLabel;
    QLabel *familyCodeLabel;
    QLabel *label_54;
    QLabel *rocLabel;
    QLabel *label_60;
    QLabel *label_64;
    QLabel *pcbLabel;
    QLabel *samLabel;
    QLabel *label_50;
    QLabel *label_58;
    QLabel *channelsLabel;
    QLabel *modelNameLabel;
    QLabel *label_52;
    QLabel *adcBitsLabel;
    QLabel *commHandle;
    QLabel *label_41;
    QLabel *label_48;
    QLabel *label_46;
    QLabel *label_66;
    QLabel *licenceLabel;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_7;
    QLabel *label_20;
    QLineEdit *xMinLineEdit;
    QLineEdit *xMaxLineEdit;
    QLabel *label_6;
    QComboBox *viewComboBox;
    QPushButton *plotPushButton;
    QSpacerItem *horizontalSpacer_2;
    QLineEdit *yMaxLineEdit;
    QLabel *label_10;
    QLineEdit *yMinLineEdit;
    QLabel *label_21;
    QLabel *label_8;
    QCheckBox *autoScaleCheckBox;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_3;
    QGridLayout *gridLayout;
    QLabel *label_2;
    QLabel *label;
    QLabel *l2numberLabel;
    QLabel *label_7;
    QLabel *label_3;
    QLabel *l2RateLabel;
    QLabel *label_5;
    QLabel *l1numberLabel;
    QLabel *elapsedTimeLabel;
    QLabel *l1RateLabel;
    QLabel *label_9;
    QLabel *receivedBuffersLabel;
    QLabel *label_4;
    QLabel *outputFileSizeLabel;
    QSpacerItem *verticalSpacer;
    QGroupBox *groupBox_8;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *saveSettingsPushButton;
    QPushButton *loadSettingsPushButton;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_8;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *startButton;
    QPushButton *resetButton;
    QWidget *tab_2;
    QGridLayout *gridLayout_5;
    QGridLayout *gridLayout_2;
    QLineEdit *triggerThresholdLineEdit;
    QLabel *label_19;
    QLabel *label_15;
    QLabel *label_11;
    QLabel *label_17;
    QLineEdit *recordLengthLineEdit;
    QLabel *label_12;
    QLabel *label_13;
    QSpacerItem *verticalSpacer_2;
    QSpinBox *postTriggerSpinBox;
    QSpinBox *adcChannelSpinBox;
    QSpinBox *dcOffsetSpinBox;
    QComboBox *triggerPolarityComboBox;
    QLabel *label_53;
    QLabel *label_55;
    QComboBox *pulsePolarityComboBox;
    QLineEdit *bltNumberLineEdit;
    QWidget *tab_3;
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox_6;
    QFormLayout *formLayout_2;
    QLabel *label_22;
    QCheckBox *lowPassFilterCheckBox;
    QLabel *label_23;
    QLineEdit *filterLenghtLineEdit;
    QLabel *label_24;
    QLineEdit *rebinLineEdit;
    QGroupBox *groupBox_7;
    QGridLayout *gridLayout_10;
    QLabel *label_25;
    QLineEdit *baseLineLineEdit;
    QLabel *label_26;
    QLineEdit *integralLengthLineEdit;
    QLabel *label_39;
    QLineEdit *pisRangeLineEdit;
    QGroupBox *groupBox_5;
    QFormLayout *formLayout_4;
    QLabel *label_32;
    QLineEdit *dscfdStartLineEdit;
    QLabel *label_33;
    QLineEdit *dcfdStopLineEdit;
    QLabel *label_34;
    QLineEdit *dcfdFractionLineEdit;
    QLabel *label_35;
    QLineEdit *dcfdThresholdLineEdit;
    QLabel *label_36;
    QLineEdit *dcfdDelayLineEdit;
    QLabel *label_37;
    QLineEdit *dcfdFilterLengthLineEdit;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout_11;
    QLabel *label_43;
    QLabel *label_27;
    QCheckBox *integralCutCheckBox;
    QLabel *label_28;
    QLineEdit *integralMinThrLineEdit;
    QLabel *label_29;
    QLineEdit *baseLineSdThresholdLineEdit;
    QCheckBox *baseLineSdCutOnCheckBox;
    QLabel *label_30;
    QLabel *label_31;
    QCheckBox *dcfdCutOnCheckBox;
    QLabel *label_38;
    QCheckBox *zcNumberCutOnCheckBox;
    QLabel *label_14;
    QCheckBox *psdCutOnCheckBox;
    QLabel *label_16;
    QLineEdit *psdMaxLineEdit;
    QLineEdit *psdMinLineEdit;
    QLabel *label_57;
    QCheckBox *rejectClippedCheckBox;
    QSpacerItem *verticalSpacer_3;
    QWidget *tab_5;
    QVBoxLayout *verticalLayout_2;
    QGroupBox *groupBox_11;
    QGridLayout *gridLayout_9;
    QLabel *label_47;
    QLabel *label_49;
    QLineEdit *aLineEdit;
    QLabel *label_51;
    QLineEdit *bLineEdit;
    QGroupBox *groupBox_10;
    QFormLayout *formLayout_5;
    QLabel *label_45;
    QLineEdit *tergetZeroLineEdit;
    QLabel *label_65;
    QLabel *label_59;
    QLineEdit *psmNbOfEnergyRangesLineEdit;
    QLabel *label_61;
    QLineEdit *psmMinEnergyLineEdit;
    QLabel *label_63;
    QLineEdit *psmMaxEnergyLineEdit;
    QLabel *label_67;
    QComboBox *comboBox;
    QTableWidget *psTableWidget;
    QLabel *label_69;
    QLineEdit *particleLineEdit;
    QPushButton *pushButton;
    QPushButton *removePushButton;
    QSpacerItem *verticalSpacer_4;
    QWidget *tab_4;
    QWidget *Info;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_68;
    QLabel *label_18;
    QSpacerItem *verticalSpacer_5;
    QMenuBar *menubar;
    QMenu *menuFile;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(996, 826);
        actionLoead_settings = new QAction(MainWindow);
        actionLoead_settings->setObjectName(QStringLiteral("actionLoead_settings"));
        actionSave_settings = new QAction(MainWindow);
        actionSave_settings->setObjectName(QStringLiteral("actionSave_settings"));
        actionSave_decay_curves = new QAction(MainWindow);
        actionSave_decay_curves->setObjectName(QStringLiteral("actionSave_decay_curves"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        horizontalLayout_3 = new QHBoxLayout(centralwidget);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        splitter = new QSplitter(centralwidget);
        splitter->setObjectName(QStringLiteral("splitter"));
        splitter->setOrientation(Qt::Horizontal);
        chartview = new QtCharts::QChartView(splitter);
        chartview->setObjectName(QStringLiteral("chartview"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(2);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(chartview->sizePolicy().hasHeightForWidth());
        chartview->setSizePolicy(sizePolicy);
        splitter->addWidget(chartview);
        tabWidget = new QTabWidget(splitter);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setMinimumSize(QSize(231, 608));
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        gridLayout_4 = new QGridLayout(tab);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        groupBox_9 = new QGroupBox(tab);
        groupBox_9->setObjectName(QStringLiteral("groupBox_9"));
        gridLayout_6 = new QGridLayout(groupBox_9);
        gridLayout_6->setObjectName(QStringLiteral("gridLayout_6"));
        label_62 = new QLabel(groupBox_9);
        label_62->setObjectName(QStringLiteral("label_62"));

        gridLayout_6->addWidget(label_62, 11, 0, 1, 1);

        amcLabel = new QLabel(groupBox_9);
        amcLabel->setObjectName(QStringLiteral("amcLabel"));

        gridLayout_6->addWidget(amcLabel, 7, 1, 1, 1);

        modelLabel = new QLabel(groupBox_9);
        modelLabel->setObjectName(QStringLiteral("modelLabel"));

        gridLayout_6->addWidget(modelLabel, 2, 1, 1, 1);

        label_44 = new QLabel(groupBox_9);
        label_44->setObjectName(QStringLiteral("label_44"));

        gridLayout_6->addWidget(label_44, 2, 0, 1, 1);

        label_40 = new QLabel(groupBox_9);
        label_40->setObjectName(QStringLiteral("label_40"));

        gridLayout_6->addWidget(label_40, 0, 0, 1, 1);

        label_56 = new QLabel(groupBox_9);
        label_56->setObjectName(QStringLiteral("label_56"));

        gridLayout_6->addWidget(label_56, 8, 0, 1, 1);

        label_42 = new QLabel(groupBox_9);
        label_42->setObjectName(QStringLiteral("label_42"));

        gridLayout_6->addWidget(label_42, 0, 1, 1, 1);

        formFactorLabel = new QLabel(groupBox_9);
        formFactorLabel->setObjectName(QStringLiteral("formFactorLabel"));

        gridLayout_6->addWidget(formFactorLabel, 4, 1, 1, 1);

        serialNumberLabel = new QLabel(groupBox_9);
        serialNumberLabel->setObjectName(QStringLiteral("serialNumberLabel"));

        gridLayout_6->addWidget(serialNumberLabel, 8, 1, 1, 1);

        familyCodeLabel = new QLabel(groupBox_9);
        familyCodeLabel->setObjectName(QStringLiteral("familyCodeLabel"));

        gridLayout_6->addWidget(familyCodeLabel, 5, 1, 1, 1);

        label_54 = new QLabel(groupBox_9);
        label_54->setObjectName(QStringLiteral("label_54"));

        gridLayout_6->addWidget(label_54, 7, 0, 1, 1);

        rocLabel = new QLabel(groupBox_9);
        rocLabel->setObjectName(QStringLiteral("rocLabel"));

        gridLayout_6->addWidget(rocLabel, 6, 1, 1, 1);

        label_60 = new QLabel(groupBox_9);
        label_60->setObjectName(QStringLiteral("label_60"));

        gridLayout_6->addWidget(label_60, 10, 0, 1, 1);

        label_64 = new QLabel(groupBox_9);
        label_64->setObjectName(QStringLiteral("label_64"));

        gridLayout_6->addWidget(label_64, 12, 0, 1, 1);

        pcbLabel = new QLabel(groupBox_9);
        pcbLabel->setObjectName(QStringLiteral("pcbLabel"));

        gridLayout_6->addWidget(pcbLabel, 9, 1, 1, 1);

        samLabel = new QLabel(groupBox_9);
        samLabel->setObjectName(QStringLiteral("samLabel"));

        gridLayout_6->addWidget(samLabel, 11, 1, 1, 1);

        label_50 = new QLabel(groupBox_9);
        label_50->setObjectName(QStringLiteral("label_50"));

        gridLayout_6->addWidget(label_50, 5, 0, 1, 1);

        label_58 = new QLabel(groupBox_9);
        label_58->setObjectName(QStringLiteral("label_58"));

        gridLayout_6->addWidget(label_58, 9, 0, 1, 1);

        channelsLabel = new QLabel(groupBox_9);
        channelsLabel->setObjectName(QStringLiteral("channelsLabel"));

        gridLayout_6->addWidget(channelsLabel, 3, 1, 1, 1);

        modelNameLabel = new QLabel(groupBox_9);
        modelNameLabel->setObjectName(QStringLiteral("modelNameLabel"));

        gridLayout_6->addWidget(modelNameLabel, 1, 1, 1, 1);

        label_52 = new QLabel(groupBox_9);
        label_52->setObjectName(QStringLiteral("label_52"));

        gridLayout_6->addWidget(label_52, 6, 0, 1, 1);

        adcBitsLabel = new QLabel(groupBox_9);
        adcBitsLabel->setObjectName(QStringLiteral("adcBitsLabel"));

        gridLayout_6->addWidget(adcBitsLabel, 10, 1, 1, 1);

        commHandle = new QLabel(groupBox_9);
        commHandle->setObjectName(QStringLiteral("commHandle"));

        gridLayout_6->addWidget(commHandle, 12, 1, 1, 1);

        label_41 = new QLabel(groupBox_9);
        label_41->setObjectName(QStringLiteral("label_41"));

        gridLayout_6->addWidget(label_41, 1, 0, 1, 1);

        label_48 = new QLabel(groupBox_9);
        label_48->setObjectName(QStringLiteral("label_48"));

        gridLayout_6->addWidget(label_48, 4, 0, 1, 1);

        label_46 = new QLabel(groupBox_9);
        label_46->setObjectName(QStringLiteral("label_46"));

        gridLayout_6->addWidget(label_46, 3, 0, 1, 1);

        label_66 = new QLabel(groupBox_9);
        label_66->setObjectName(QStringLiteral("label_66"));

        gridLayout_6->addWidget(label_66, 13, 0, 1, 1);

        licenceLabel = new QLabel(groupBox_9);
        licenceLabel->setObjectName(QStringLiteral("licenceLabel"));

        gridLayout_6->addWidget(licenceLabel, 13, 1, 1, 1);


        gridLayout_4->addWidget(groupBox_9, 2, 0, 1, 1);

        groupBox_3 = new QGroupBox(tab);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        gridLayout_7 = new QGridLayout(groupBox_3);
        gridLayout_7->setObjectName(QStringLiteral("gridLayout_7"));
        label_20 = new QLabel(groupBox_3);
        label_20->setObjectName(QStringLiteral("label_20"));

        gridLayout_7->addWidget(label_20, 3, 0, 1, 1);

        xMinLineEdit = new QLineEdit(groupBox_3);
        xMinLineEdit->setObjectName(QStringLiteral("xMinLineEdit"));

        gridLayout_7->addWidget(xMinLineEdit, 1, 1, 1, 1);

        xMaxLineEdit = new QLineEdit(groupBox_3);
        xMaxLineEdit->setObjectName(QStringLiteral("xMaxLineEdit"));

        gridLayout_7->addWidget(xMaxLineEdit, 2, 1, 1, 1);

        label_6 = new QLabel(groupBox_3);
        label_6->setObjectName(QStringLiteral("label_6"));

        gridLayout_7->addWidget(label_6, 0, 0, 1, 1);

        viewComboBox = new QComboBox(groupBox_3);
        viewComboBox->addItem(QString());
        viewComboBox->addItem(QString());
        viewComboBox->addItem(QString());
        viewComboBox->addItem(QString());
        viewComboBox->addItem(QString());
        viewComboBox->addItem(QString());
        viewComboBox->addItem(QString());
        viewComboBox->addItem(QString());
        viewComboBox->addItem(QString());
        viewComboBox->setObjectName(QStringLiteral("viewComboBox"));

        gridLayout_7->addWidget(viewComboBox, 0, 1, 1, 1);

        plotPushButton = new QPushButton(groupBox_3);
        plotPushButton->setObjectName(QStringLiteral("plotPushButton"));

        gridLayout_7->addWidget(plotPushButton, 6, 1, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_7->addItem(horizontalSpacer_2, 6, 0, 1, 1);

        yMaxLineEdit = new QLineEdit(groupBox_3);
        yMaxLineEdit->setObjectName(QStringLiteral("yMaxLineEdit"));

        gridLayout_7->addWidget(yMaxLineEdit, 4, 1, 1, 1);

        label_10 = new QLabel(groupBox_3);
        label_10->setObjectName(QStringLiteral("label_10"));

        gridLayout_7->addWidget(label_10, 2, 0, 1, 1);

        yMinLineEdit = new QLineEdit(groupBox_3);
        yMinLineEdit->setObjectName(QStringLiteral("yMinLineEdit"));

        gridLayout_7->addWidget(yMinLineEdit, 3, 1, 1, 1);

        label_21 = new QLabel(groupBox_3);
        label_21->setObjectName(QStringLiteral("label_21"));

        gridLayout_7->addWidget(label_21, 4, 0, 1, 1);

        label_8 = new QLabel(groupBox_3);
        label_8->setObjectName(QStringLiteral("label_8"));

        gridLayout_7->addWidget(label_8, 1, 0, 1, 1);

        autoScaleCheckBox = new QCheckBox(groupBox_3);
        autoScaleCheckBox->setObjectName(QStringLiteral("autoScaleCheckBox"));

        gridLayout_7->addWidget(autoScaleCheckBox, 5, 1, 1, 1);


        gridLayout_4->addWidget(groupBox_3, 3, 0, 1, 1);

        groupBox = new QGroupBox(tab);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        gridLayout_3 = new QGridLayout(groupBox);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 2, 0, 1, 1);

        l2numberLabel = new QLabel(groupBox);
        l2numberLabel->setObjectName(QStringLiteral("l2numberLabel"));

        gridLayout->addWidget(l2numberLabel, 2, 1, 1, 1);

        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QStringLiteral("label_7"));

        gridLayout->addWidget(label_7, 4, 0, 1, 1);

        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout->addWidget(label_3, 5, 0, 1, 1);

        l2RateLabel = new QLabel(groupBox);
        l2RateLabel->setObjectName(QStringLiteral("l2RateLabel"));

        gridLayout->addWidget(l2RateLabel, 4, 1, 1, 1);

        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout->addWidget(label_5, 3, 0, 1, 1);

        l1numberLabel = new QLabel(groupBox);
        l1numberLabel->setObjectName(QStringLiteral("l1numberLabel"));

        gridLayout->addWidget(l1numberLabel, 1, 1, 1, 1);

        elapsedTimeLabel = new QLabel(groupBox);
        elapsedTimeLabel->setObjectName(QStringLiteral("elapsedTimeLabel"));

        gridLayout->addWidget(elapsedTimeLabel, 5, 1, 1, 1);

        l1RateLabel = new QLabel(groupBox);
        l1RateLabel->setObjectName(QStringLiteral("l1RateLabel"));

        gridLayout->addWidget(l1RateLabel, 3, 1, 1, 1);

        label_9 = new QLabel(groupBox);
        label_9->setObjectName(QStringLiteral("label_9"));

        gridLayout->addWidget(label_9, 0, 0, 1, 1);

        receivedBuffersLabel = new QLabel(groupBox);
        receivedBuffersLabel->setObjectName(QStringLiteral("receivedBuffersLabel"));

        gridLayout->addWidget(receivedBuffersLabel, 0, 1, 1, 1);

        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout->addWidget(label_4, 6, 0, 1, 1);

        outputFileSizeLabel = new QLabel(groupBox);
        outputFileSizeLabel->setObjectName(QStringLiteral("outputFileSizeLabel"));

        gridLayout->addWidget(outputFileSizeLabel, 6, 1, 1, 1);


        gridLayout_3->addLayout(gridLayout, 1, 0, 1, 1);


        gridLayout_4->addWidget(groupBox, 1, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_4->addItem(verticalSpacer, 9, 0, 1, 1);

        groupBox_8 = new QGroupBox(tab);
        groupBox_8->setObjectName(QStringLiteral("groupBox_8"));
        horizontalLayout_2 = new QHBoxLayout(groupBox_8);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_3);

        saveSettingsPushButton = new QPushButton(groupBox_8);
        saveSettingsPushButton->setObjectName(QStringLiteral("saveSettingsPushButton"));

        horizontalLayout_2->addWidget(saveSettingsPushButton);

        loadSettingsPushButton = new QPushButton(groupBox_8);
        loadSettingsPushButton->setObjectName(QStringLiteral("loadSettingsPushButton"));

        horizontalLayout_2->addWidget(loadSettingsPushButton);


        gridLayout_4->addWidget(groupBox_8, 5, 0, 1, 1);

        groupBox_2 = new QGroupBox(tab);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        gridLayout_8 = new QGridLayout(groupBox_2);
        gridLayout_8->setObjectName(QStringLiteral("gridLayout_8"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        startButton = new QPushButton(groupBox_2);
        startButton->setObjectName(QStringLiteral("startButton"));

        horizontalLayout->addWidget(startButton);

        resetButton = new QPushButton(groupBox_2);
        resetButton->setObjectName(QStringLiteral("resetButton"));

        horizontalLayout->addWidget(resetButton);


        gridLayout_8->addLayout(horizontalLayout, 0, 0, 1, 1);


        gridLayout_4->addWidget(groupBox_2, 0, 0, 1, 1);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        gridLayout_5 = new QGridLayout(tab_2);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        triggerThresholdLineEdit = new QLineEdit(tab_2);
        triggerThresholdLineEdit->setObjectName(QStringLiteral("triggerThresholdLineEdit"));

        gridLayout_2->addWidget(triggerThresholdLineEdit, 2, 1, 1, 1);

        label_19 = new QLabel(tab_2);
        label_19->setObjectName(QStringLiteral("label_19"));

        gridLayout_2->addWidget(label_19, 0, 0, 1, 1);

        label_15 = new QLabel(tab_2);
        label_15->setObjectName(QStringLiteral("label_15"));

        gridLayout_2->addWidget(label_15, 6, 0, 1, 1);

        label_11 = new QLabel(tab_2);
        label_11->setObjectName(QStringLiteral("label_11"));

        gridLayout_2->addWidget(label_11, 2, 0, 1, 1);

        label_17 = new QLabel(tab_2);
        label_17->setObjectName(QStringLiteral("label_17"));

        gridLayout_2->addWidget(label_17, 7, 0, 1, 1);

        recordLengthLineEdit = new QLineEdit(tab_2);
        recordLengthLineEdit->setObjectName(QStringLiteral("recordLengthLineEdit"));

        gridLayout_2->addWidget(recordLengthLineEdit, 1, 1, 1, 1);

        label_12 = new QLabel(tab_2);
        label_12->setObjectName(QStringLiteral("label_12"));

        gridLayout_2->addWidget(label_12, 1, 0, 1, 1);

        label_13 = new QLabel(tab_2);
        label_13->setObjectName(QStringLiteral("label_13"));

        gridLayout_2->addWidget(label_13, 5, 0, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer_2, 9, 1, 1, 1);

        postTriggerSpinBox = new QSpinBox(tab_2);
        postTriggerSpinBox->setObjectName(QStringLiteral("postTriggerSpinBox"));

        gridLayout_2->addWidget(postTriggerSpinBox, 6, 1, 1, 1);

        adcChannelSpinBox = new QSpinBox(tab_2);
        adcChannelSpinBox->setObjectName(QStringLiteral("adcChannelSpinBox"));
        adcChannelSpinBox->setMaximum(7);

        gridLayout_2->addWidget(adcChannelSpinBox, 0, 1, 1, 1);

        dcOffsetSpinBox = new QSpinBox(tab_2);
        dcOffsetSpinBox->setObjectName(QStringLiteral("dcOffsetSpinBox"));
        dcOffsetSpinBox->setMaximum(1000000);

        gridLayout_2->addWidget(dcOffsetSpinBox, 5, 1, 1, 1);

        triggerPolarityComboBox = new QComboBox(tab_2);
        triggerPolarityComboBox->addItem(QString());
        triggerPolarityComboBox->addItem(QString());
        triggerPolarityComboBox->setObjectName(QStringLiteral("triggerPolarityComboBox"));

        gridLayout_2->addWidget(triggerPolarityComboBox, 3, 1, 1, 1);

        label_53 = new QLabel(tab_2);
        label_53->setObjectName(QStringLiteral("label_53"));

        gridLayout_2->addWidget(label_53, 3, 0, 1, 1);

        label_55 = new QLabel(tab_2);
        label_55->setObjectName(QStringLiteral("label_55"));

        gridLayout_2->addWidget(label_55, 4, 0, 1, 1);

        pulsePolarityComboBox = new QComboBox(tab_2);
        pulsePolarityComboBox->addItem(QString());
        pulsePolarityComboBox->addItem(QString());
        pulsePolarityComboBox->setObjectName(QStringLiteral("pulsePolarityComboBox"));

        gridLayout_2->addWidget(pulsePolarityComboBox, 4, 1, 1, 1);

        bltNumberLineEdit = new QLineEdit(tab_2);
        bltNumberLineEdit->setObjectName(QStringLiteral("bltNumberLineEdit"));

        gridLayout_2->addWidget(bltNumberLineEdit, 7, 1, 1, 1);


        gridLayout_5->addLayout(gridLayout_2, 0, 0, 1, 1);

        tabWidget->addTab(tab_2, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QStringLiteral("tab_3"));
        verticalLayout = new QVBoxLayout(tab_3);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        groupBox_6 = new QGroupBox(tab_3);
        groupBox_6->setObjectName(QStringLiteral("groupBox_6"));
        formLayout_2 = new QFormLayout(groupBox_6);
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        label_22 = new QLabel(groupBox_6);
        label_22->setObjectName(QStringLiteral("label_22"));

        formLayout_2->setWidget(0, QFormLayout::LabelRole, label_22);

        lowPassFilterCheckBox = new QCheckBox(groupBox_6);
        lowPassFilterCheckBox->setObjectName(QStringLiteral("lowPassFilterCheckBox"));

        formLayout_2->setWidget(0, QFormLayout::FieldRole, lowPassFilterCheckBox);

        label_23 = new QLabel(groupBox_6);
        label_23->setObjectName(QStringLiteral("label_23"));

        formLayout_2->setWidget(1, QFormLayout::LabelRole, label_23);

        filterLenghtLineEdit = new QLineEdit(groupBox_6);
        filterLenghtLineEdit->setObjectName(QStringLiteral("filterLenghtLineEdit"));

        formLayout_2->setWidget(1, QFormLayout::FieldRole, filterLenghtLineEdit);

        label_24 = new QLabel(groupBox_6);
        label_24->setObjectName(QStringLiteral("label_24"));

        formLayout_2->setWidget(2, QFormLayout::LabelRole, label_24);

        rebinLineEdit = new QLineEdit(groupBox_6);
        rebinLineEdit->setObjectName(QStringLiteral("rebinLineEdit"));

        formLayout_2->setWidget(2, QFormLayout::FieldRole, rebinLineEdit);


        verticalLayout->addWidget(groupBox_6);

        groupBox_7 = new QGroupBox(tab_3);
        groupBox_7->setObjectName(QStringLiteral("groupBox_7"));
        gridLayout_10 = new QGridLayout(groupBox_7);
        gridLayout_10->setObjectName(QStringLiteral("gridLayout_10"));
        label_25 = new QLabel(groupBox_7);
        label_25->setObjectName(QStringLiteral("label_25"));

        gridLayout_10->addWidget(label_25, 0, 0, 1, 1);

        baseLineLineEdit = new QLineEdit(groupBox_7);
        baseLineLineEdit->setObjectName(QStringLiteral("baseLineLineEdit"));

        gridLayout_10->addWidget(baseLineLineEdit, 0, 1, 1, 1);

        label_26 = new QLabel(groupBox_7);
        label_26->setObjectName(QStringLiteral("label_26"));

        gridLayout_10->addWidget(label_26, 1, 0, 1, 1);

        integralLengthLineEdit = new QLineEdit(groupBox_7);
        integralLengthLineEdit->setObjectName(QStringLiteral("integralLengthLineEdit"));

        gridLayout_10->addWidget(integralLengthLineEdit, 1, 1, 1, 1);

        label_39 = new QLabel(groupBox_7);
        label_39->setObjectName(QStringLiteral("label_39"));

        gridLayout_10->addWidget(label_39, 2, 0, 1, 1);

        pisRangeLineEdit = new QLineEdit(groupBox_7);
        pisRangeLineEdit->setObjectName(QStringLiteral("pisRangeLineEdit"));

        gridLayout_10->addWidget(pisRangeLineEdit, 2, 1, 1, 1);


        verticalLayout->addWidget(groupBox_7);

        groupBox_5 = new QGroupBox(tab_3);
        groupBox_5->setObjectName(QStringLiteral("groupBox_5"));
        formLayout_4 = new QFormLayout(groupBox_5);
        formLayout_4->setObjectName(QStringLiteral("formLayout_4"));
        label_32 = new QLabel(groupBox_5);
        label_32->setObjectName(QStringLiteral("label_32"));

        formLayout_4->setWidget(0, QFormLayout::LabelRole, label_32);

        dscfdStartLineEdit = new QLineEdit(groupBox_5);
        dscfdStartLineEdit->setObjectName(QStringLiteral("dscfdStartLineEdit"));

        formLayout_4->setWidget(0, QFormLayout::FieldRole, dscfdStartLineEdit);

        label_33 = new QLabel(groupBox_5);
        label_33->setObjectName(QStringLiteral("label_33"));

        formLayout_4->setWidget(1, QFormLayout::LabelRole, label_33);

        dcfdStopLineEdit = new QLineEdit(groupBox_5);
        dcfdStopLineEdit->setObjectName(QStringLiteral("dcfdStopLineEdit"));

        formLayout_4->setWidget(1, QFormLayout::FieldRole, dcfdStopLineEdit);

        label_34 = new QLabel(groupBox_5);
        label_34->setObjectName(QStringLiteral("label_34"));

        formLayout_4->setWidget(2, QFormLayout::LabelRole, label_34);

        dcfdFractionLineEdit = new QLineEdit(groupBox_5);
        dcfdFractionLineEdit->setObjectName(QStringLiteral("dcfdFractionLineEdit"));

        formLayout_4->setWidget(2, QFormLayout::FieldRole, dcfdFractionLineEdit);

        label_35 = new QLabel(groupBox_5);
        label_35->setObjectName(QStringLiteral("label_35"));

        formLayout_4->setWidget(3, QFormLayout::LabelRole, label_35);

        dcfdThresholdLineEdit = new QLineEdit(groupBox_5);
        dcfdThresholdLineEdit->setObjectName(QStringLiteral("dcfdThresholdLineEdit"));

        formLayout_4->setWidget(3, QFormLayout::FieldRole, dcfdThresholdLineEdit);

        label_36 = new QLabel(groupBox_5);
        label_36->setObjectName(QStringLiteral("label_36"));

        formLayout_4->setWidget(4, QFormLayout::LabelRole, label_36);

        dcfdDelayLineEdit = new QLineEdit(groupBox_5);
        dcfdDelayLineEdit->setObjectName(QStringLiteral("dcfdDelayLineEdit"));

        formLayout_4->setWidget(4, QFormLayout::FieldRole, dcfdDelayLineEdit);

        label_37 = new QLabel(groupBox_5);
        label_37->setObjectName(QStringLiteral("label_37"));

        formLayout_4->setWidget(5, QFormLayout::LabelRole, label_37);

        dcfdFilterLengthLineEdit = new QLineEdit(groupBox_5);
        dcfdFilterLengthLineEdit->setObjectName(QStringLiteral("dcfdFilterLengthLineEdit"));

        formLayout_4->setWidget(5, QFormLayout::FieldRole, dcfdFilterLengthLineEdit);


        verticalLayout->addWidget(groupBox_5);

        groupBox_4 = new QGroupBox(tab_3);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        gridLayout_11 = new QGridLayout(groupBox_4);
        gridLayout_11->setObjectName(QStringLiteral("gridLayout_11"));
        label_43 = new QLabel(groupBox_4);
        label_43->setObjectName(QStringLiteral("label_43"));

        gridLayout_11->addWidget(label_43, 9, 0, 1, 1);

        label_27 = new QLabel(groupBox_4);
        label_27->setObjectName(QStringLiteral("label_27"));

        gridLayout_11->addWidget(label_27, 1, 0, 1, 1);

        integralCutCheckBox = new QCheckBox(groupBox_4);
        integralCutCheckBox->setObjectName(QStringLiteral("integralCutCheckBox"));

        gridLayout_11->addWidget(integralCutCheckBox, 1, 1, 1, 1);

        label_28 = new QLabel(groupBox_4);
        label_28->setObjectName(QStringLiteral("label_28"));

        gridLayout_11->addWidget(label_28, 2, 0, 1, 1);

        integralMinThrLineEdit = new QLineEdit(groupBox_4);
        integralMinThrLineEdit->setObjectName(QStringLiteral("integralMinThrLineEdit"));

        gridLayout_11->addWidget(integralMinThrLineEdit, 2, 1, 1, 1);

        label_29 = new QLabel(groupBox_4);
        label_29->setObjectName(QStringLiteral("label_29"));

        gridLayout_11->addWidget(label_29, 3, 0, 1, 1);

        baseLineSdThresholdLineEdit = new QLineEdit(groupBox_4);
        baseLineSdThresholdLineEdit->setObjectName(QStringLiteral("baseLineSdThresholdLineEdit"));

        gridLayout_11->addWidget(baseLineSdThresholdLineEdit, 4, 1, 1, 1);

        baseLineSdCutOnCheckBox = new QCheckBox(groupBox_4);
        baseLineSdCutOnCheckBox->setObjectName(QStringLiteral("baseLineSdCutOnCheckBox"));

        gridLayout_11->addWidget(baseLineSdCutOnCheckBox, 3, 1, 1, 1);

        label_30 = new QLabel(groupBox_4);
        label_30->setObjectName(QStringLiteral("label_30"));

        gridLayout_11->addWidget(label_30, 4, 0, 1, 1);

        label_31 = new QLabel(groupBox_4);
        label_31->setObjectName(QStringLiteral("label_31"));

        gridLayout_11->addWidget(label_31, 5, 0, 1, 1);

        dcfdCutOnCheckBox = new QCheckBox(groupBox_4);
        dcfdCutOnCheckBox->setObjectName(QStringLiteral("dcfdCutOnCheckBox"));

        gridLayout_11->addWidget(dcfdCutOnCheckBox, 5, 1, 1, 1);

        label_38 = new QLabel(groupBox_4);
        label_38->setObjectName(QStringLiteral("label_38"));

        gridLayout_11->addWidget(label_38, 6, 0, 1, 1);

        zcNumberCutOnCheckBox = new QCheckBox(groupBox_4);
        zcNumberCutOnCheckBox->setObjectName(QStringLiteral("zcNumberCutOnCheckBox"));

        gridLayout_11->addWidget(zcNumberCutOnCheckBox, 6, 1, 1, 1);

        label_14 = new QLabel(groupBox_4);
        label_14->setObjectName(QStringLiteral("label_14"));

        gridLayout_11->addWidget(label_14, 7, 0, 1, 1);

        psdCutOnCheckBox = new QCheckBox(groupBox_4);
        psdCutOnCheckBox->setObjectName(QStringLiteral("psdCutOnCheckBox"));

        gridLayout_11->addWidget(psdCutOnCheckBox, 7, 1, 1, 1);

        label_16 = new QLabel(groupBox_4);
        label_16->setObjectName(QStringLiteral("label_16"));

        gridLayout_11->addWidget(label_16, 8, 0, 1, 1);

        psdMaxLineEdit = new QLineEdit(groupBox_4);
        psdMaxLineEdit->setObjectName(QStringLiteral("psdMaxLineEdit"));

        gridLayout_11->addWidget(psdMaxLineEdit, 9, 1, 1, 1);

        psdMinLineEdit = new QLineEdit(groupBox_4);
        psdMinLineEdit->setObjectName(QStringLiteral("psdMinLineEdit"));

        gridLayout_11->addWidget(psdMinLineEdit, 8, 1, 1, 1);

        label_57 = new QLabel(groupBox_4);
        label_57->setObjectName(QStringLiteral("label_57"));

        gridLayout_11->addWidget(label_57, 0, 0, 1, 1);

        rejectClippedCheckBox = new QCheckBox(groupBox_4);
        rejectClippedCheckBox->setObjectName(QStringLiteral("rejectClippedCheckBox"));

        gridLayout_11->addWidget(rejectClippedCheckBox, 0, 1, 1, 1);


        verticalLayout->addWidget(groupBox_4);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_3);

        tabWidget->addTab(tab_3, QString());
        tab_5 = new QWidget();
        tab_5->setObjectName(QStringLiteral("tab_5"));
        verticalLayout_2 = new QVBoxLayout(tab_5);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        groupBox_11 = new QGroupBox(tab_5);
        groupBox_11->setObjectName(QStringLiteral("groupBox_11"));
        gridLayout_9 = new QGridLayout(groupBox_11);
        gridLayout_9->setObjectName(QStringLiteral("gridLayout_9"));
        label_47 = new QLabel(groupBox_11);
        label_47->setObjectName(QStringLiteral("label_47"));

        gridLayout_9->addWidget(label_47, 0, 1, 1, 1);

        label_49 = new QLabel(groupBox_11);
        label_49->setObjectName(QStringLiteral("label_49"));

        gridLayout_9->addWidget(label_49, 1, 0, 1, 1);

        aLineEdit = new QLineEdit(groupBox_11);
        aLineEdit->setObjectName(QStringLiteral("aLineEdit"));

        gridLayout_9->addWidget(aLineEdit, 1, 1, 1, 1);

        label_51 = new QLabel(groupBox_11);
        label_51->setObjectName(QStringLiteral("label_51"));

        gridLayout_9->addWidget(label_51, 2, 0, 1, 1);

        bLineEdit = new QLineEdit(groupBox_11);
        bLineEdit->setObjectName(QStringLiteral("bLineEdit"));

        gridLayout_9->addWidget(bLineEdit, 2, 1, 1, 1);


        verticalLayout_2->addWidget(groupBox_11);

        groupBox_10 = new QGroupBox(tab_5);
        groupBox_10->setObjectName(QStringLiteral("groupBox_10"));
        formLayout_5 = new QFormLayout(groupBox_10);
        formLayout_5->setObjectName(QStringLiteral("formLayout_5"));
        label_45 = new QLabel(groupBox_10);
        label_45->setObjectName(QStringLiteral("label_45"));

        formLayout_5->setWidget(0, QFormLayout::LabelRole, label_45);

        tergetZeroLineEdit = new QLineEdit(groupBox_10);
        tergetZeroLineEdit->setObjectName(QStringLiteral("tergetZeroLineEdit"));

        formLayout_5->setWidget(0, QFormLayout::FieldRole, tergetZeroLineEdit);

        label_65 = new QLabel(groupBox_10);
        label_65->setObjectName(QStringLiteral("label_65"));

        formLayout_5->setWidget(1, QFormLayout::LabelRole, label_65);

        label_59 = new QLabel(groupBox_10);
        label_59->setObjectName(QStringLiteral("label_59"));

        formLayout_5->setWidget(4, QFormLayout::LabelRole, label_59);

        psmNbOfEnergyRangesLineEdit = new QLineEdit(groupBox_10);
        psmNbOfEnergyRangesLineEdit->setObjectName(QStringLiteral("psmNbOfEnergyRangesLineEdit"));

        formLayout_5->setWidget(4, QFormLayout::FieldRole, psmNbOfEnergyRangesLineEdit);

        label_61 = new QLabel(groupBox_10);
        label_61->setObjectName(QStringLiteral("label_61"));

        formLayout_5->setWidget(5, QFormLayout::LabelRole, label_61);

        psmMinEnergyLineEdit = new QLineEdit(groupBox_10);
        psmMinEnergyLineEdit->setObjectName(QStringLiteral("psmMinEnergyLineEdit"));

        formLayout_5->setWidget(5, QFormLayout::FieldRole, psmMinEnergyLineEdit);

        label_63 = new QLabel(groupBox_10);
        label_63->setObjectName(QStringLiteral("label_63"));

        formLayout_5->setWidget(7, QFormLayout::LabelRole, label_63);

        psmMaxEnergyLineEdit = new QLineEdit(groupBox_10);
        psmMaxEnergyLineEdit->setObjectName(QStringLiteral("psmMaxEnergyLineEdit"));

        formLayout_5->setWidget(7, QFormLayout::FieldRole, psmMaxEnergyLineEdit);

        label_67 = new QLabel(groupBox_10);
        label_67->setObjectName(QStringLiteral("label_67"));

        formLayout_5->setWidget(10, QFormLayout::LabelRole, label_67);

        comboBox = new QComboBox(groupBox_10);
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->setObjectName(QStringLiteral("comboBox"));

        formLayout_5->setWidget(10, QFormLayout::FieldRole, comboBox);

        psTableWidget = new QTableWidget(groupBox_10);
        if (psTableWidget->columnCount() < 4)
            psTableWidget->setColumnCount(4);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        psTableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        psTableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        psTableWidget->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        psTableWidget->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        psTableWidget->setObjectName(QStringLiteral("psTableWidget"));

        formLayout_5->setWidget(2, QFormLayout::SpanningRole, psTableWidget);

        label_69 = new QLabel(groupBox_10);
        label_69->setObjectName(QStringLiteral("label_69"));

        formLayout_5->setWidget(3, QFormLayout::LabelRole, label_69);

        particleLineEdit = new QLineEdit(groupBox_10);
        particleLineEdit->setObjectName(QStringLiteral("particleLineEdit"));

        formLayout_5->setWidget(3, QFormLayout::FieldRole, particleLineEdit);

        pushButton = new QPushButton(groupBox_10);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        formLayout_5->setWidget(9, QFormLayout::FieldRole, pushButton);

        removePushButton = new QPushButton(groupBox_10);
        removePushButton->setObjectName(QStringLiteral("removePushButton"));

        formLayout_5->setWidget(8, QFormLayout::FieldRole, removePushButton);


        verticalLayout_2->addWidget(groupBox_10);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_4);

        tabWidget->addTab(tab_5, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QStringLiteral("tab_4"));
        tabWidget->addTab(tab_4, QString());
        Info = new QWidget();
        Info->setObjectName(QStringLiteral("Info"));
        verticalLayout_3 = new QVBoxLayout(Info);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        label_68 = new QLabel(Info);
        label_68->setObjectName(QStringLiteral("label_68"));

        verticalLayout_3->addWidget(label_68);

        label_18 = new QLabel(Info);
        label_18->setObjectName(QStringLiteral("label_18"));

        verticalLayout_3->addWidget(label_18);

        verticalSpacer_5 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_5);

        tabWidget->addTab(Info, QString());
        splitter->addWidget(tabWidget);

        horizontalLayout_3->addWidget(splitter);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 996, 15));
        menuFile = new QMenu(menubar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        MainWindow->setStatusBar(statusbar);

        menubar->addAction(menuFile->menuAction());
        menuFile->addAction(actionSave_decay_curves);
        menuFile->addAction(actionLoead_settings);
        menuFile->addAction(actionSave_settings);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(2);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "veroDigitizer 1.8", nullptr));
        actionLoead_settings->setText(QApplication::translate("MainWindow", "Load settings", nullptr));
        actionSave_settings->setText(QApplication::translate("MainWindow", "Save settings", nullptr));
        actionSave_decay_curves->setText(QApplication::translate("MainWindow", "Save decay curves", nullptr));
        groupBox_9->setTitle(QApplication::translate("MainWindow", "Digitizer", nullptr));
        label_62->setText(QApplication::translate("MainWindow", "SAM Data Loaded:", nullptr));
        amcLabel->setText(QApplication::translate("MainWindow", "-", nullptr));
        modelLabel->setText(QApplication::translate("MainWindow", "-", nullptr));
        label_44->setText(QApplication::translate("MainWindow", "Model:", nullptr));
        label_40->setText(QApplication::translate("MainWindow", "Status:", nullptr));
        label_56->setText(QApplication::translate("MainWindow", "Serial number:", nullptr));
        label_42->setText(QApplication::translate("MainWindow", "Idle", nullptr));
        formFactorLabel->setText(QApplication::translate("MainWindow", "-", nullptr));
        serialNumberLabel->setText(QApplication::translate("MainWindow", "-", nullptr));
        familyCodeLabel->setText(QApplication::translate("MainWindow", "-", nullptr));
        label_54->setText(QApplication::translate("MainWindow", "AMC Firmware:", nullptr));
        rocLabel->setText(QApplication::translate("MainWindow", "-", nullptr));
        label_60->setText(QApplication::translate("MainWindow", "ADC bits:", nullptr));
        label_64->setText(QApplication::translate("MainWindow", "Comm handle:", nullptr));
        pcbLabel->setText(QApplication::translate("MainWindow", "-", nullptr));
        samLabel->setText(QApplication::translate("MainWindow", "-", nullptr));
        label_50->setText(QApplication::translate("MainWindow", "Family code:", nullptr));
        label_58->setText(QApplication::translate("MainWindow", "PCB revision:", nullptr));
        channelsLabel->setText(QApplication::translate("MainWindow", "-", nullptr));
        modelNameLabel->setText(QApplication::translate("MainWindow", "-", nullptr));
        label_52->setText(QApplication::translate("MainWindow", "ROC Firmware:", nullptr));
        adcBitsLabel->setText(QApplication::translate("MainWindow", "-", nullptr));
        commHandle->setText(QApplication::translate("MainWindow", "-", nullptr));
        label_41->setText(QApplication::translate("MainWindow", "Model name:", nullptr));
        label_48->setText(QApplication::translate("MainWindow", "Form Factor", nullptr));
        label_46->setText(QApplication::translate("MainWindow", "Channels:", nullptr));
        label_66->setText(QApplication::translate("MainWindow", "Licence:", nullptr));
        licenceLabel->setText(QApplication::translate("MainWindow", "-", nullptr));
        groupBox_3->setTitle(QApplication::translate("MainWindow", "Display", nullptr));
        label_20->setText(QApplication::translate("MainWindow", "Y min", nullptr));
        xMinLineEdit->setText(QApplication::translate("MainWindow", "0", nullptr));
        xMaxLineEdit->setText(QApplication::translate("MainWindow", "8000", nullptr));
        label_6->setText(QApplication::translate("MainWindow", "View:", nullptr));
        viewComboBox->setItemText(0, QApplication::translate("MainWindow", "Raw waveform", nullptr));
        viewComboBox->setItemText(1, QApplication::translate("MainWindow", "Decimated waveform", nullptr));
        viewComboBox->setItemText(2, QApplication::translate("MainWindow", "DCFD response", nullptr));
        viewComboBox->setItemText(3, QApplication::translate("MainWindow", "Integral histogram (PHS)", nullptr));
        viewComboBox->setItemText(4, QApplication::translate("MainWindow", "Energy spectrum (calibrated)", nullptr));
        viewComboBox->setItemText(5, QApplication::translate("MainWindow", "Avarage pulse shape", nullptr));
        viewComboBox->setItemText(6, QApplication::translate("MainWindow", "Energy sorted pulse shapes", nullptr));
        viewComboBox->setItemText(7, QApplication::translate("MainWindow", "Pulse shape factor (A/Q) 2D", nullptr));
        viewComboBox->setItemText(8, QApplication::translate("MainWindow", "Fast/slow ratio 2D", nullptr));

        plotPushButton->setText(QApplication::translate("MainWindow", "Plot", nullptr));
        yMaxLineEdit->setText(QApplication::translate("MainWindow", "100", nullptr));
        label_10->setText(QApplication::translate("MainWindow", "X max", nullptr));
        yMinLineEdit->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_21->setText(QApplication::translate("MainWindow", "Y max", nullptr));
        label_8->setText(QApplication::translate("MainWindow", "X min", nullptr));
        autoScaleCheckBox->setText(QApplication::translate("MainWindow", "Auto scale", nullptr));
        groupBox->setTitle(QApplication::translate("MainWindow", "Run status", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "Hardware triggers (L1):", nullptr));
        label->setText(QApplication::translate("MainWindow", "Software triggers(L2):", nullptr));
        l2numberLabel->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_7->setText(QApplication::translate("MainWindow", "L2 rate (Hz)", nullptr));
        label_3->setText(QApplication::translate("MainWindow", "Time elapsed (s)", nullptr));
        l2RateLabel->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_5->setText(QApplication::translate("MainWindow", "L1 rate (Hz)", nullptr));
        l1numberLabel->setText(QApplication::translate("MainWindow", "0", nullptr));
        elapsedTimeLabel->setText(QApplication::translate("MainWindow", "0", nullptr));
        l1RateLabel->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_9->setText(QApplication::translate("MainWindow", "Buffers received:", nullptr));
        receivedBuffersLabel->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_4->setText(QApplication::translate("MainWindow", "Output file size (MB)", nullptr));
        outputFileSizeLabel->setText(QApplication::translate("MainWindow", "0", nullptr));
        groupBox_8->setTitle(QApplication::translate("MainWindow", "Settings", nullptr));
        saveSettingsPushButton->setText(QApplication::translate("MainWindow", "Save", nullptr));
        loadSettingsPushButton->setText(QApplication::translate("MainWindow", "Load", nullptr));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "Run control", nullptr));
        startButton->setText(QApplication::translate("MainWindow", "Start", nullptr));
        resetButton->setText(QApplication::translate("MainWindow", "Reset", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("MainWindow", "Status&&control", nullptr));
        label_19->setText(QApplication::translate("MainWindow", "Channel", nullptr));
        label_15->setText(QApplication::translate("MainWindow", "Post trigger size", nullptr));
        label_11->setText(QApplication::translate("MainWindow", "Trigger threshold", nullptr));
        label_17->setText(QApplication::translate("MainWindow", "Max number of events per BLT", nullptr));
        recordLengthLineEdit->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_12->setText(QApplication::translate("MainWindow", "Record length", nullptr));
        label_13->setText(QApplication::translate("MainWindow", "DC offset", nullptr));
        triggerPolarityComboBox->setItemText(0, QApplication::translate("MainWindow", "Falling Edge", nullptr));
        triggerPolarityComboBox->setItemText(1, QApplication::translate("MainWindow", "Rising Edge", nullptr));

        label_53->setText(QApplication::translate("MainWindow", "Trigger polarity", nullptr));
        label_55->setText(QApplication::translate("MainWindow", "Pulse polarity", nullptr));
        pulsePolarityComboBox->setItemText(0, QApplication::translate("MainWindow", "Negative", nullptr));
        pulsePolarityComboBox->setItemText(1, QApplication::translate("MainWindow", "Positive", nullptr));

        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("MainWindow", "L1 settings", nullptr));
        groupBox_6->setTitle(QApplication::translate("MainWindow", "Decimation", nullptr));
        label_22->setText(QApplication::translate("MainWindow", "Low pass filter:", nullptr));
        lowPassFilterCheckBox->setText(QApplication::translate("MainWindow", "Enable", nullptr));
        label_23->setText(QApplication::translate("MainWindow", "Filter length:", nullptr));
        filterLenghtLineEdit->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_24->setText(QApplication::translate("MainWindow", "Rebin:", nullptr));
        rebinLineEdit->setText(QApplication::translate("MainWindow", "1", nullptr));
        groupBox_7->setTitle(QApplication::translate("MainWindow", "Pulse Integral Spectrum", nullptr));
        label_25->setText(QApplication::translate("MainWindow", "Base line length:", nullptr));
        baseLineLineEdit->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_26->setText(QApplication::translate("MainWindow", "Integral length:", nullptr));
        integralLengthLineEdit->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_39->setText(QApplication::translate("MainWindow", "Spectrum range:", nullptr));
        pisRangeLineEdit->setText(QApplication::translate("MainWindow", "0", nullptr));
        groupBox_5->setTitle(QApplication::translate("MainWindow", "DCFD settings", nullptr));
        label_32->setText(QApplication::translate("MainWindow", "Start:", nullptr));
        label_33->setText(QApplication::translate("MainWindow", "Stop:", nullptr));
        label_34->setText(QApplication::translate("MainWindow", "Fraction:", nullptr));
        label_35->setText(QApplication::translate("MainWindow", "Threshold:", nullptr));
        label_36->setText(QApplication::translate("MainWindow", "Delay:", nullptr));
        label_37->setText(QApplication::translate("MainWindow", "Filter length:", nullptr));
        groupBox_4->setTitle(QApplication::translate("MainWindow", "Quality cuts", nullptr));
        label_43->setText(QApplication::translate("MainWindow", "PSD max:", nullptr));
        label_27->setText(QApplication::translate("MainWindow", "Integral cut", nullptr));
        integralCutCheckBox->setText(QApplication::translate("MainWindow", "Enable", nullptr));
        label_28->setText(QApplication::translate("MainWindow", "Minimum integral threshold:", nullptr));
        label_29->setText(QApplication::translate("MainWindow", "BL std. dev. cut", nullptr));
        baseLineSdCutOnCheckBox->setText(QApplication::translate("MainWindow", "Enable", nullptr));
        label_30->setText(QApplication::translate("MainWindow", "BL std. dev. threshold", nullptr));
        label_31->setText(QApplication::translate("MainWindow", "DCFD cut:", nullptr));
        dcfdCutOnCheckBox->setText(QApplication::translate("MainWindow", "Enable", nullptr));
        label_38->setText(QApplication::translate("MainWindow", "ZC number cut:", nullptr));
        zcNumberCutOnCheckBox->setText(QApplication::translate("MainWindow", "Enable", nullptr));
        label_14->setText(QApplication::translate("MainWindow", "PSD cut:", nullptr));
        psdCutOnCheckBox->setText(QApplication::translate("MainWindow", "Enable", nullptr));
        label_16->setText(QApplication::translate("MainWindow", "PSD min:", nullptr));
        psdMinLineEdit->setText(QString());
        label_57->setText(QApplication::translate("MainWindow", "Reject clipped events:", nullptr));
        rejectClippedCheckBox->setText(QApplication::translate("MainWindow", "Enable", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QApplication::translate("MainWindow", "L2 settings", nullptr));
        groupBox_11->setTitle(QApplication::translate("MainWindow", "Energy Spectrum", nullptr));
        label_47->setText(QApplication::translate("MainWindow", "Calibration curve: Energy= a * channel + b", nullptr));
        label_49->setText(QApplication::translate("MainWindow", "a:", nullptr));
        label_51->setText(QApplication::translate("MainWindow", "b:", nullptr));
        groupBox_10->setTitle(QApplication::translate("MainWindow", "Pulse Shape Measurement", nullptr));
        label_45->setText(QApplication::translate("MainWindow", "Target zero position (samples):", nullptr));
        label_65->setText(QApplication::translate("MainWindow", "Pulse shape definitions:", nullptr));
        label_59->setText(QApplication::translate("MainWindow", "Number of integral bins:", nullptr));
        psmNbOfEnergyRangesLineEdit->setText(QApplication::translate("MainWindow", "10", nullptr));
        label_61->setText(QApplication::translate("MainWindow", "Minimum integral (channels):", nullptr));
        psmMinEnergyLineEdit->setText(QApplication::translate("MainWindow", "100", nullptr));
        label_63->setText(QApplication::translate("MainWindow", "Maximum integrals (channels):", nullptr));
        psmMaxEnergyLineEdit->setText(QApplication::translate("MainWindow", "1000", nullptr));
        label_67->setText(QApplication::translate("MainWindow", "Normalization:", nullptr));
        comboBox->setItemText(0, QApplication::translate("MainWindow", "Not normalized", nullptr));
        comboBox->setItemText(1, QApplication::translate("MainWindow", "Normalized to maximum [0-1]", nullptr));
        comboBox->setItemText(2, QApplication::translate("MainWindow", "Normalized to integral = 1", nullptr));

        QTableWidgetItem *___qtablewidgetitem = psTableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("MainWindow", "Particle", nullptr));
        QTableWidgetItem *___qtablewidgetitem1 = psTableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("MainWindow", "Bins", nullptr));
        QTableWidgetItem *___qtablewidgetitem2 = psTableWidget->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QApplication::translate("MainWindow", "Imin", nullptr));
        QTableWidgetItem *___qtablewidgetitem3 = psTableWidget->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QApplication::translate("MainWindow", "Imax", nullptr));
        label_69->setText(QApplication::translate("MainWindow", "Particle:", nullptr));
        particleLineEdit->setText(QApplication::translate("MainWindow", "0", nullptr));
        pushButton->setText(QApplication::translate("MainWindow", "Add", nullptr));
        removePushButton->setText(QApplication::translate("MainWindow", "Remove", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_5), QApplication::translate("MainWindow", "Analysis", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_4), QApplication::translate("MainWindow", "Data saving", nullptr));
        label_68->setText(QApplication::translate("MainWindow", "(C) W. Wolszczak 2018", nullptr));
        label_18->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/img/logo-tu-delft_0.5.jpg\"/></p></body></html>", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(Info), QApplication::translate("MainWindow", "Info", nullptr));
        menuFile->setTitle(QApplication::translate("MainWindow", "File", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
