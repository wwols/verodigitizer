#include "EventAnalyzer.h"
#include <iostream>

using namespace std;
EventAnalyzer::EventAnalyzer(){
}

EventAnalyzer::~EventAnalyzer(){
  //delete[] yAv;
  //delete[] xAllignedAv;
  //delete[] yAllignedAv;
  //delete[] xStDev;
  //delete[] yStDev;
  //delete[] avPulseCounter;
  delete dcfd;
  //delete[] avPulseGraph;
  //for(Int_t i = 0; i<eRangeNb+1+aRangeNb; i++)
    //delete avPulseVec[i];
  //delete[] avPulseVec;
}

void EventAnalyzer::CanRebin(TH1* h){
#if ROOT_VERSION_CODE < ROOT_VERSION(6,0,0)
  h->SetBit(TH1::kCanRebin);
#else
  h->SetCanExtend(TH1::kXaxis);
#endif
}

double EventAnalyzer::stDev(int length, double average, double* array){ //uint16_t* array
  double stdev = 0;
  for(int i = 0; i<length; i++) {
    stdev+=((double)array[i]-average)*((double)array[i]-average);//(bitRange - Waveform[i]-baseLine)*(bitRange-Waveform[i]-baseLine);
  }
  stdev/=length-1;
  stdev = sqrt(stdev);
  return stdev;
}

template<class T>
void EventAnalyzer::twoPoleFilter(int length, T* array){
  double* y1 = new double[length];
  //fc=0.2 2 pole low pas chebyshev
  /*
    double a0 = 1.997396E-01;
    double a1 = 3.994792E-01;
    double a2 = 1.997396E-01;
    double b1 = 4.291048E-01;
    double b2 = -2.280633E-01;*/

  //fc = 0.05 2 pole low pas chebyshev
  /* double a0 = 1.868823E-02;
    double a1 = 3.737647E-02;
    double a2 = 1.868823E-02;
    double b1 = 1.593937E+00;
    double b2 = -6.686903E-01;
  */


  //fc = 0.01 2 pole low pas chebyshev
  double a0 = 8.663387E-04;
  double a1 = 1.732678E-03;
  double a2 = 8.663387E-04;
  double b1 = 1.919129E+00;
  double b2 = -9.225943E-01;

  y1[0] = array[0];
  y1[1] = array[1];
  y1[2] = array[2];
  for(int i = 2; i<length; i++) {
    y1[i] = a0*array[i] + a1*array[i-1] + a2*array[i-2] + b1*y1[i-1] + b2*y1[i-2];
  }
  for(int i = 0; i<length; i++) array[i] = y1[i];
  delete[] y1;
}

template<class T>
void EventAnalyzer::movingAverageFilter(int length, T* array, int M){ //even M
  double* y1 = new double[length];
  double acc = 0;

  for(int i = 0; i<M+1; i++){
    acc += array[i];
  }
  y1[M/2] = acc / (M+1);

  for(int i = M/2+1; i< length - M/2-1; i++){
    acc = acc + array[i+M/2] - array[i-M/2-1];
    y1[i] = acc/(M+1);
  }
  for(int i = M/2; i<length-M/2-1; i++) array[i] = y1[i];
  delete[] y1;
}

void EventAnalyzer::loadSettings(Json::Value root, uint32_t recLen){
  cout<<"Loading settings..."<<endl;
  m_root = root;
  //cout<<"Record length is: "<<recordLength<<endl;
  pulsesNb = root["eventsNb"].asInt();//0 means all events
  energyRange = root["energyRange"].asDouble();//400e3;
  psdRange = root["psdRange"].asDouble();// 0.01;
  eRangeNb = root["eRangeNb"].asInt();//10; //number of energy sub-ranges
  aRangeNb = root["aRangeNb"].asInt();//10; //number of alpha window energy sub-ranges
  plotOn = 0;

  //Data settings
  //Int_t postTrigger = 91; //%83.2
  //bitRange = root["bitRange"].asInt(); //1023;
  samplingRate = 0.25; //ns
  triggerResolution = 16/samplingRate;
  //cout<<"Trigger Resolution "<<triggerResolution<<endl;
  // baseLineLength = root["baseLineLength"].asInt();// 836; //samples

  //Energy calibration settings
  aCoeff = root["EnergyCalibration"]["a"].asDouble();
  bCoeff = root["EnergyCalibration"]["b"].asDouble();
  e.setLinCalib(aCoeff,bCoeff);

  //DSP settings
  lowPassFilterOn =  root["analysis"]["lowPassFilterOn"].asInt();
  movingAverageLength = root["analysis"]["movingAverageLength"].asInt();

  //Cuts settings
  integralMinThreshold = root["analysis"]["qualityCuts"]["integralMinThreshold"].asInt();
  dcfdCutOn = root["analysis"]["qualityCuts"]["dcfdCutOn"].asInt();
  integralCutOn = root["analysis"]["qualityCuts"]["integralCutOn"].asInt();
  baseLineSdCutOn = root["analysis"]["qualityCuts"]["baseLineSdCutOn"].asInt();
  puRejectionOn = root["analysis"]["qualityCuts"]["puRejectionOn"].asInt();
  psdCutOn = root["analysis"]["qualityCuts"]["psdCutOn"].asInt();
  zcNumberCutOn = root["analysis"]["qualityCuts"]["zcNumberCutOn"].asInt();

  puGateRatio = root["analysis"]["qualityCuts"]["puGateRatio"].asDouble();
  puMinThreshold = root["analysis"]["qualityCuts"]["puMinThreshold"].asDouble();
  psdMaxThreshold = root["analysis"]["qualityCuts"]["psdMaxThreshold"].asDouble();//0.008;
  psdMinThreshold = root["analysis"]["qualityCuts"]["psdMinThreshold"].asDouble();

  //integralMinThreshold = root["cuts"]["integralMinThreshold"].asDouble();//8e3;
  blSigmaThreshold = root["analysis"]["qualityCuts"]["baseLineSdThreshold"].asDouble();//2.6; //1
  alphaEnergyMin = root["cuts"]["alphaEnergyMin"].asDouble();
  alphaEnergyMax  = root["cuts"]["alphaEnergyMax"].asDouble();
  alphaPsdMin = root["cuts"]["alphaPsdMin"].asDouble();
  alphaPsdMax  = root["cuts"]["alphaPsdMax"].asDouble();

  //CFD settings
  dcfdLength = root["analysis"]["dcfdSettings"]["length"].asInt(); //20; //moving average length
  dcfdDelay = root["analysis"]["dcfdSettings"]["delay"].asInt(); //10; //delay
  dcfdFraction = root["analysis"]["dcfdSettings"]["fraction"].asDouble(); //0.2; //fraction
  dcfdThreshold = root["daq"]["pulsePolarity"].asInt()*root["analysis"]["dcfdSettings"]["threshold"].asDouble(); //threshold 5
  dcfdStart = root["analysis"]["dcfdSettings"]["start"].asInt();
  dcfdStop = root["analysis"]["dcfdSettings"]["stop"].asInt();


  //cut flags
  avSamples = 0;
  rejectEnergy = 0;
  rejectBL = 0;
  rejectPSD = 0;
  rejectDCFD = 0;
  alphaTag = 0;

  //position where dcfd will allign zero-crossing type in the target array
  targetZeroPos = root["targetZeroPos"].asInt(); //2000;

  //output plots settings
  yMin = root["outputPlots"]["yMin"].asDouble();//0.008;
  yMax = root["outputPlots"]["yMax"].asDouble();//8e3;

  //data recording settings
  //std::string outputFileName = root.get("outputFileName", "UTF-8" ).asString(); //output root file nam
  std::string outputFileName = root["dataRecording"]["outputFileName"].asString(); //output root file nam


  saveRawWaveform = root["dataRecording"]["saveRawWaveform"].asInt();
  saveDecimatedWaveform = root["dataRecording"]["saveDecimatedWaveform"].asInt();
  
  dspFilteringOn = root["dataRecording"]["dspFilteringOn"].asInt();
  dcfdOn = root["dataRecording"]["dcfdOn"].asInt();
  patternComparatorOn = root["dataRecording"]["paterComparatorOn"].asInt();

  //daq settings
  channelIndex  = root["daq"]["channelIndex"].asInt();
  baseLineLength  = root["analysis"]["baseLineLength"].asInt();
  integralLength = root["analysis"]["integralLength"].asInt();
  rebin = root["analysis"]["rebin"].asInt();
  // integralMinThreshold = root["daq"]["integralTriggerThreshold"].asInt();

  cout<<"recLen: "<<recLen<<" rebin: "<<rebin<<endl;
  recordLength = (int)recLen/rebin;
  cout<<"Analysis record length is: "<<recordLength<<endl;


  //first plotting
  first = 0;
  ploted = 0;

  xAllignedAv = new Double_t[bufferLength];
  dcfdOutputLength = dcfdStop-dcfdStart;

  dcfd = new Dcfd(dcfdLength,dcfdDelay,dcfdFraction,decimatedWaveform,dcfdStart,dcfdStop,dcfdThreshold);


  f = new TFile(outputFileName.c_str(),"RECREATE");
  hTotal   = new TH1F("hTotal","Integral spectrum",energyBins,0,energyRange);
  hEnergy   = new TH1F("hEnergy","Energy spectrum",energyBins,0,e.eCalib(energyRange));
  //hFast   = new TH1F("hFast","Fast energy spectrum",energyBins,0,energyRange);
  //hSlow   = new TH1F("hLong","Slow energy spectrum",energyBins,0,energyRange);
  //hTail   = new TH1F("hTail","Tail energy spectrum",energyBins,0,energyRange);
  hPSD = new TH2F("hPSD","Pulse shape factor vs energy",root["plottingSettings"]["psd"]["xbins"].asInt(),
		  root["plottingSettings"]["psd"]["xmin"].asDouble(),
                    root["plottingSettings"]["psd"]["xmax"].asDouble(),
                    root["plottingSettings"]["psd"]["ybins"].asInt(),
                    root["plottingSettings"]["psd"]["ymin"].asDouble(),
                    root["plottingSettings"]["psd"]["ymax"].asDouble());
    hPuFactor = new TH2F("hPuFactor","Pile-up factor vs energy",root["plottingSettings"]["pu"]["xbins"].asInt(),
                         root["plottingSettings"]["pu"]["xmin"].asDouble(),
                         root["plottingSettings"]["pu"]["xmax"].asDouble(),
                         root["plottingSettings"]["pu"]["ybins"].asInt(),
                         root["plottingSettings"]["pu"]["ymin"].asDouble(),
                         root["plottingSettings"]["pu"]["ymax"].asDouble());

    hCuts = new TH1F("hCuts","Quality cuts",3,0,3);
    hCuts->SetStats(0);
    hCuts->SetFillColor(38);
    CanRebin(hCuts);// hCuts->SetBit(TH1::kCanRebin);
    hCuts->LabelsDeflate();

    incrementalHistogram.clear();
    for(int i = 0; i<10; i++){
      std::string s = "hIncremental"+std::to_string(i);
      char const* pchar = s.c_str();
      incrementalHistogram.push_back(
      new TH1F(pchar,"Incremental histogram",energyBins,0,energyRange));
    }


    avPulseGraph = new TGraph[10];// +1 for overflow bin

    tree = new TTree("tree","Events");
    tree->Branch("energy",&energy,"energy/D");
    tree->Branch("baseLine",&baseLine,"baseLine/D");
    tree->Branch("psd",&psd,"psd/D");
    tree->Branch("chi2",&chi2,"chi2/D");
    tree->Branch("dcfdZC",&dcfdZC,"dcfdZC/D");
    tree->Branch("puFactor",&puFactor,"puFactor/D");

    if(saveRawWaveform) {
      tree->Branch("rawLength",&rawLength,"rawLength/I");
      tree->Branch("rawWaveform",rawWaveform,"rawWaveform[rawLength]/D"); //tree->Branch("Waveform",Waveform,"Waveform[NSamples]/s");
    }

    if(saveDecimatedWaveform) {
      tree->Branch("decimatedLength",&decimatedLength,"decimatedLength/I"); //i - 32bit unsigned integer
      tree->Branch("decimatedWaveform",decimatedWaveform,"decimatedWaveform[decimatedLength]/D"); //tree->Branch("Waveform",Waveform,"Waveform[NSamples]/s");
    }

    if(root["dataRecording"]["saveDcfdWaveform"].asInt()) {
      tree->Branch("dcfdOutputLength",&dcfdOutputLength,"dcfdOutputLength/I");
      tree->Branch("dcfdArray",dcfdArray,"dcfdArray[dcfdOutputLength]/D");
    }

    tree->Branch("zcNumber",&zcNumber,"zcNumber/I");

    if(dspFilteringOn) tree->Branch("dspEnergy",&dspEnergy,"dspEnergy/D");
    const char* patternFileName = "pattern1.txt";
    if(patternComparatorOn){
      cout<<"Pattern file name: "<<patternFileName<<endl;
      patternFile = fopen(patternFileName,"r");
      if(patternFile==NULL) {
        cout<<"Pattern file loading... failed!"<<endl;
        //goto error;
      }
      fscanf(patternFile, "%hu", &patternLength);
      cout<<"Pattern length: "<<patternLength<<endl;

      if(patternLength!=0) pattern = new Double_t[patternLength];
      char buf[255];
      Int_t i = 0;
      while(fgets(buf,sizeof buf, patternFile) != NULL){
        double sample;
        sscanf(buf, "%lf", &sample);
        pattern[i]=sample;
        i++;
      }
    }
    rawLength = recordLength*rebin;
    filteredWaveform = new double[recordLength*rebin];
    decimatedLength = recordLength;
    pisArray = new double[getPisLength()];
    cout<<"decimated length "<<decimatedLength<<endl;
    pulseShapeManager = new PulseShapeManager(e);

    int polarity = -1;
    pulseShapeManager->reset();
    pulseShapeManager->clear();
    for(int i = 0; i<m_root["analysis"]["PulseShapes"].size(); i++){
      pulseShapeManager->addPulseShape(polarity, decimatedWaveform, decimatedLength,
                                       m_root["analysis"]["PulseShapes"][i]["particle"].asInt(),
          m_root["analysis"]["PulseShapes"][i]["bins"].asInt(),
          m_root["analysis"]["PulseShapes"][i]["Imin"].asInt(),
          m_root["analysis"]["PulseShapes"][i]["Imax"].asInt(), targetZeroPos);
      std::cout<<"PulseShapeManager::addPulseShape i="<<i<<std::endl;
    }
    cout<<"Objects has been created and initialized!"<<endl;
  //reset();
}



void EventAnalyzer::fillIncrementalHistogram(){
 double tempIntegral = 0;

 for(int i = 0; i<10; i++){
  int start = (int)dcfdZC+30 + 50*i;
  int stop = start + 50;
  for(int number=start;number<stop;number++)
    tempIntegral += m_root["daq"]["pulsePolarity"].asInt()*((double)decimatedWaveform[number]-(double)baseLine);

  incrementalHistogram[i]->Fill(tempIntegral);
  }
}

void EventAnalyzer::createGraphs(){
  for(int psIndex = 0; psIndex<pulseShapeManager->getPulseShapesNumber();psIndex++){
      std::string s = std::to_string(psIndex);
      char const *pchar = s.c_str();
      TCanvas *c1 = new TCanvas(pchar,pchar,1600,1200);
      for(int i = 0; i<recordLength; i++) xAllignedAv[i]=i;
      for(int i=0; i< pulseShapeManager->getIntegralBinsNumber(psIndex); i++){
          std::cout<<i<<" recordlenght:"<<recordLength<<" "<<pulseShapeManager->getPulseShape(psIndex,i,(char*)"0-1")<<std::endl;

          if(pulseShapeManager->getPulseShape(psIndex,i,(char*)"0-1") != NULL){
              avPulseGraph[i] = TGraph(recordLength,xAllignedAv,pulseShapeManager->getPulseShape(psIndex,i,(char*)"0-1"));
              avPulseGraph[i].SetLineColor(i%8+1);
              avPulseGraph[i].GetYaxis()->SetRangeUser(yMin,yMax);//
              if(i==0){
                  avPulseGraph[i].SetTitle(";Time (samples);Normalized intensity (arb. units)");
                  avPulseGraph[i].Draw("AL");
                }
              else avPulseGraph[i].Draw("SAME");
            }
        }
      Double_t rangeWidth = pulseShapeManager->getBinWidth(psIndex);//(energyRange-integralMinThreshold)/10;
      double iMin = pulseShapeManager->getImin(psIndex);

      TLegend* leg = new TLegend(0.1,0.7,0.48,0.9);
      TString* energyString = new TString[pulseShapeManager->getIntegralBinsNumber(psIndex)];
      for(Int_t i=0;i<pulseShapeManager->getIntegralBinsNumber(psIndex);i++){
          energyString[i].Form("%.0f-%.0f keV",e.eCalib(i*rangeWidth+iMin), e.eCalib((i+1)*rangeWidth+iMin));
          leg->AddEntry(&avPulseGraph[i],energyString[i],"l");
        }
      //energyString[10].Form(">%.0f keV", e.eCalib(10*rangeWidth));
      //leg->AddEntry(&avPulseGraph[10],energyString[10],"l");
      leg->SetNColumns(5);
      leg->Draw();

      c1->Modified();
      c1->Update();
      c1->Write();
      delete leg;
      delete[] energyString;
      delete c1;
    }
}

double EventAnalyzer::average(double * array, int length){ //short unsigned int * array
  double average = 0;
  for(int i = 1;i<length;i++){ //start from one
    average += (Double_t)(array[i]); //16383 for 14-bit
  }
  average /= (Double_t)length-1; //start from one
  return average;
}

double EventAnalyzer::getBaseLine(){
  return baseLine;
}

void EventAnalyzer::analyze(CAEN_DGTZ_UINT16_EVENT_t* event){
  //Wsize=event->ChSize[channelIndex]; //pcTime=runningTime;
  if(!event) return;
  energy = 0;
  baseLine = 0;
  chi2 = 0;
  dspEnergy = 0;
  clipped = 0;
  rejectClipped = 0;
  long int maxValue = (long int)pow(2,bitRange)-1; //not a good place

  for(int i=0; i<(int)recordLength*rebin; i++) {
    filteredWaveform[i] = event->DataChannel[channelIndex][i];
    rawWaveform[i] = filteredWaveform[i];

    if((rawWaveform[i]==maxValue)||(rawWaveform[i]==0)){
      if(m_root["analysis"]["qualityCuts"]["rejectClipped"].asInt() == 1){
          rejectClipped = 1;
          return;
        }
      }
    else rejectClipped = 0;

  }

  //twoPoleFilter(recordLength*rebin, tempArray2);
  if(lowPassFilterOn) movingAverageFilter(recordLength*rebin, filteredWaveform, movingAverageLength); //88
  //  for(int i=0; i<recordLength; i++) Waveform[i] = tempArray2[i*rebin];
  //delete tempArray2;

  for(int i=0; i<(int)recordLength*rebin; i++) {
    int bin = (int) i / rebin;
    if(i%rebin == 0) decimatedWaveform[bin] = 0;
    if(i%rebin == 0 && bin>0) decimatedWaveform[bin-1]/=rebin;
    decimatedWaveform[bin]+=filteredWaveform[i];
    //if(Waveform[i]==0) clipped = 1;
  }
  //delete tempArray2;

  //    twoPoleFilter(recordLength, Waveform);
  baseLine = average(decimatedWaveform,baseLineLength);

  double sd = stDev(baseLineLength,baseLine,decimatedWaveform);
  if(baseLineSdCutOn == 1)
    if(sd<blSigmaThreshold) rejectBL = 0; else {
      rejectBL = 1;
      return;
    }
  else rejectBL = 0;

  for(int number=baseLineLength;number<integralLength+baseLineLength;number++){
    energy += m_root["daq"]["pulsePolarity"].asInt()*((double)decimatedWaveform[number]-(double)baseLine);
  }

  if(integralCutOn == 1)
    if(energy>integralMinThreshold) rejectEnergy = 0; else {
      rejectEnergy = 1;
      return;
    }
  else rejectEnergy = 0;

  dcfd->processSignal(baseLine);
  std::vector<double> zcVec = dcfd->getZeroCrossingVec();
  if(zcVec.size() > 0) dcfdZC = zcVec[0]; else dcfdZC = 0;
  zcNumber = zcVec.size();
  //cout<<"Zero crossing "<<dcfdZC<<endl;
  if (zcVec.size() == 0) cout<<"<--------------------------------------------------------- No Zero Crossing!"<<endl;
  double* tempArray = dcfd->getOutputSignal();
  for(Int_t sample = 0; sample<dcfdOutputLength; sample++){ dcfdArray[sample]=tempArray[sample]; }
  //delate tempArray;


  double g1 = 0;
  double g2 = 0;
  for(int i = baseLineLength; i<baseLineLength+integralLength; i++){
    double value = m_root["daq"]["pulsePolarity"].asInt()*((double)decimatedWaveform[i]-(double)baseLine);
    if(i<baseLineLength+puGateRatio*integralLength) g1 += value;  else g2 += value;
  }
  puFactor = g1/g2;
  //hPuFactor->Fill(energy,puFactor);

  if(puRejectionOn == 1)
    if(puFactor>puMinThreshold) rejectPu = 0; else {
      rejectPu = 1;
      return;
    }
  else rejectPu = 0;

  if(dcfdCutOn == 1)
    if(zcVec.size()>0) rejectDCFD = 0; else {
      rejectDCFD = 1;
      return;
    }
  else rejectDCFD = 0;
/*
  unsigned int minIndex = baseLineLength;
  double psdIntegral = 0;
  for(unsigned int number=(int)dcfdStart; number<(int)dcfdStop; number++){
    if (-1*m_root["daq"]["pulsePolarity"].asInt()*decimatedWaveform[number]<-1*m_root["daq"]["pulsePolarity"].asInt()*decimatedWaveform[minIndex]) minIndex=number;
    psdIntegral += m_root["daq"]["pulsePolarity"].asInt()*((double)decimatedWaveform[number]-(double)baseLine); // (baseLine-(double)decimatedWaveform[number]);
  }
  psd = m_root["daq"]["pulsePolarity"].asInt()*((double)decimatedWaveform[minIndex]-(double)baseLine)/psdIntegral; //(baseLine-(double)decimatedWaveform[minIndex])/psdIntegral;

  if(psdCutOn == 1)
    if(psd<psdMaxThreshold && psd>psdMinThreshold) rejectPSD = 0; else {
      rejectPSD = 1;
      return;
    }
  else rejectPSD = 0;
*/
  if(zcNumberCutOn == 1)
    if(zcNumber==1) rejectZcNumber = 0; else {
      rejectZcNumber = 1;
      return;
    }
  else rejectZcNumber = 0;
}

int EventAnalyzer::fill(){
  if(psd>alphaPsdMin && psd<alphaPsdMax && energy > alphaEnergyMin && energy<alphaEnergyMax) alphaTag = 1; else alphaTag = 0;
  //WARNING <----------The following line is used for debuging
  alphaTag=0;

  if(rejectClipped) hCuts->Fill("rejectClipped",1);
  if(rejectEnergy) hCuts->Fill("rejectEnergy",1);
  if(rejectDCFD) hCuts->Fill("rejectDCFD",1);
  if(rejectBL) hCuts->Fill("rejectBL",1);
  if(rejectPu) hCuts->Fill("rejectPU",1);
  if(rejectPSD) hCuts->Fill("rejectPSD",1);
  if(rejectZcNumber) hCuts->Fill("rejectZcNumber",1);

  if(!(rejectEnergy+rejectBL+rejectDCFD+rejectPu+rejectPSD+rejectZcNumber+rejectClipped)){
    hPuFactor->Fill(energy,puFactor);
    //hFast->Fill(fastEnergy);
    //hSlow->Fill(slowEnergy);
    //hTail->Fill(tailEnergy);
    tree->Fill();
    hTotal->Fill(energy);
    hEnergy->Fill(e.eCalib(energy));
    hPSD->Fill(energy,psd);
    fillIncrementalHistogram();
  }
  int triggerDecision = rejectEnergy+rejectBL+rejectDCFD+rejectPu+rejectPSD+rejectZcNumber+rejectClipped;
  if(triggerDecision==0) pulseShapeManager->addWaveform(0,baseLine,energy,dcfdZC);
  return triggerDecision;
}

void EventAnalyzer::save(){
  cout<<"Saving output data..."<<endl;
  if(tree){
    tree->Write(tree->GetName());
  }
  if(f){
    f->Write();}
  saveIncrementalHistograms();
}


int EventAnalyzer::saveAscii(char * fileName){
  pulseShapeManager->saveAscii(fileName);
  return 0;
}

int EventAnalyzer::saveIncrementalHistograms(char * asciiFileName){
  FILE *fileAscii; ///tudelft.net/staff-bulk/tnw/rrrrid/rdm/data/lusci/liveView.txt
  //char * asciiFileName = "kotyzaploty.txt";
  try{
    fileAscii = fopen(asciiFileName,"w");
    if(fileAscii){


      for (int sample = 0; sample < energyBins; sample++){
        for (Int_t i=0; i<10; i++) {
          fprintf(fileAscii,"%E \t %E \t",incrementalHistogram[i]->GetBinCenter(sample) ,incrementalHistogram[i]->GetBinContent(sample)); //previous format %011.2f \t
        }
        fprintf(fileAscii,"\r\n");
      }
      fclose(fileAscii);
    }
  }
  catch(...){
    cout<<"Error: unable to save live view file!"<<endl;
  }
  return 0;
}
