#ifndef EVENTANALYZER_H
#define EVENTANALYZER_H

//#include <iostream>
#include <TCanvas.h>
#include <TLegend.h>
#include <TGraph.h>
#include <TTree.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <EnergyCalibrator.h>
#include <dcfd.h>
#include <CAENDigitizer.h>
#include <json/json.h>
#include <pulseshapemanager.h>

//template<class T>
//template<class T2>

//template<class T>
class EventAnalyzer{
 private:
  static const int energyBins = 2048;
  static const int bufferLength = 448000;

  uint32_t recordLength;
  int pulsesNb, eRangeNb, aRangeNb, plotOn, bitRange, triggerResolution, baseLineLength, dcfdLength, dcfdDelay, dcfdStart, dcfdStop, zcNumber;
  int avSamples, rejectClipped, rejectEnergy, rejectBL, rejectPSD, rejectDCFD, clipped, alphaTag, targetZeroPos;
  int first, ploted, saveRawWaveform, saveDecimatedWaveform, dcfdOutputLength, integralLength, channelIndex, movingAverageLength;
  int patternLength, patternComparatorOn, dspFilteringOn, dcfdOn, dcfdCutOn, integralCutOn, baseLineSdCutOn, puRejectionOn, psdCutOn, rejectPu, rejectZcNumber, zcNumberCutOn;
  int rebin, lowPassFilterOn;
  int decimatedLength;
  int rawLength;

  double puFactor, puGateRatio, puMinThreshold;
  double energyRange, psdRange, samplingRate, aCoeff, bCoeff, psdMaxThreshold, psdMinThreshold, energyMinThreshold, blSigmaThreshold, alphaEnergyMin, alphaEnergyMax, alphaPsdMin, alphaPsdMax;
  double psd, chi2, dspEnergy, dcfdZC, dcfdFraction, dcfdThreshold, yMin, yMax;
  //double fastEnergy, slowEnergy, tailEnergy;
  double integralMinThreshold, energy, baseLine;

  double* pisArray;
  double* pattern;
  double* filteredWaveform;
  double rawWaveform[bufferLength];
  double decimatedWaveform[bufferLength];
  double dcfdArray[bufferLength];
  Double_t* xAllignedAv;


  Dcfd* dcfd;
  TFile *f;
  TTree *tree;
  TH1F* hTotal;
  TH1F* hEnergy;
  TH2F *hPSD;
  TH2F *hPuFactor;
  TH1F *hCuts;
  TGraph* avPulseGraph;
  FILE * patternFile;
  EnergyCalibrator e;
  PulseShapeManager *pulseShapeManager;
  std::vector<TH1F*> incrementalHistogram;
  Json::Value m_root;

 public:
  EventAnalyzer();
  ~EventAnalyzer();

  template<class T>
  void twoPoleFilter(int length, T* array);
  void CanRebin(TH1* h);

  template<class T>
  void movingAverageFilter(int length, T* array, int M);

  double stDev(int length, double average, double* array);
  //void allignPulse();
  void loadSettings(Json::Value root, uint32_t recLen);
  //void normalizePulses();
  void filterPulses();
  void createGraphs();
  double average(double * array, int length);
  double getBaseLine();
  void analyze(CAEN_DGTZ_UINT16_EVENT_t* event);
  int fill();
  void save();
  int saveAscii(char * asciiFileName = (char*)"default");
  void reset(){pulseShapeManager->reset();}
  double* getRawWaveform() {
    return rawWaveform;
  }
  double* getDecimatedWaveform(){
    return decimatedWaveform;
  }
  double* getDcfdWaveform(){
    return dcfdArray;
  }

  double* getPulseShape(int psIndex, int integralIndex){
    return pulseShapeManager->getPulseShape(psIndex,integralIndex);
  }

  Int_t getDecimatedWaveformLength(){
    return decimatedLength;
  }
  Int_t getDcfdWaveformLength(){
    return dcfdOutputLength;
  }

  int getPulseShapeLength(){
     return recordLength;
  }

  int getPisLength(){
    return hTotal->GetNbinsX();
  }

  double* getPis(){
    for(int i = 0; i<getPisLength(); i++){
        pisArray[i]=hTotal->GetBinContent(i);
      }
    return pisArray;
  }

  void fillIncrementalHistogram();
  int saveIncrementalHistograms(char * asciiFileName = (char*) "defaultIncremental.txt");

  void setBitRange(int ADC_Nbits){
    bitRange = ADC_Nbits;
  }
};


#endif
