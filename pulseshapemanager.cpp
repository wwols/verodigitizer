#include "pulseshapemanager.h"
#include <PulseShape.h>

PulseShapeManager::PulseShapeManager(EnergyCalibrator e):e(e){};


/*
void PulseShapeManager::loadSettings(){

}
*/

void PulseShapeManager::addPulseShape(int polarity, double* waveform, long int waveformLength,int particleIndex,
                   int nbIntegralBins,double integralMin,double integralMax, double targetZeroPosition){
  pulseShapeVector.push_back(new PulseShape(e, polarity, waveform, waveformLength, particleIndex,
                                        nbIntegralBins, integralMin, integralMax, targetZeroPosition));

}

void PulseShapeManager::addWaveform(int particleIndex, double baseLine, double integral, double zeroCrossingTime){
  for(std::vector<PulseShape*>::iterator it = pulseShapeVector.begin(); it!=pulseShapeVector.end(); it++)
    (*it)->addWaveform(particleIndex, baseLine, integral, zeroCrossingTime);
}

double* PulseShapeManager::getPulseShape(int psIndex, int integralIndex, char* option){
  std::cout<<"getPulseShape"<<std::endl;
  if(pulseShapeVector.size()>0){
      std::string opt = option;
      std::size_t found = opt.find("non");
      if(found != std::string::npos)
        return pulseShapeVector[psIndex]->getPulseShape(integralIndex);
      else if(opt.find("0-1") != std::string::npos){
          std::cout<<"Pieja kury pieja!"<<std::endl;
          return pulseShapeVector[psIndex]->getPulseShape(integralIndex,(char*)"0-1");
        }
      else if(opt.find("integral") != std::string::npos)
        return pulseShapeVector[psIndex]->getPulseShape(integralIndex,1.0);
    }
  else
    return NULL;
}

void PulseShapeManager::resetCounters(){
  for(std::vector<PulseShape*>::iterator it = pulseShapeVector.begin(); it!=pulseShapeVector.end(); it++)
    (*it)->resetCounters();
}

void PulseShapeManager::clearBuffers(){
  for(std::vector<PulseShape*>::iterator it = pulseShapeVector.begin(); it!=pulseShapeVector.end(); it++)
    (*it)->clearBuffers();
}

void PulseShapeManager::reset(){
  resetCounters();
  clearBuffers();
}

void PulseShapeManager::clear(){
  for(std::vector<PulseShape*>::iterator it = pulseShapeVector.begin(); it!=pulseShapeVector.end(); it++)
    delete *it;
  pulseShapeVector.clear();
}

int PulseShapeManager::saveAscii(char * fileName){
  std::cout<<"pulseShapeVectorSize="<<pulseShapeVector.size()<<std::endl;
  for(int i = 0; i < pulseShapeVector.size(); i++){
      char buffer [64];
      sprintf(buffer,"%s_%d.txt",fileName,i);
      std::cout<<"i="<<i<<std::endl;
      pulseShapeVector[i]->saveAscii(buffer);
    }
  return 0;
}


/*
void PulseShapeManager::filterPulses(){
  for(int i=0; i<eRangeNb + 1 + aRangeNb; i++){
    twoPoleFilter((int)recordLength,avPulseVec[i]);
  }
}
*/

