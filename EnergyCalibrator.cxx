#include "EnergyCalibrator.h"

EnergyCalibrator::EnergyCalibrator(){
  a=0;
  b=0;
}

EnergyCalibrator::~EnergyCalibrator(){
}

void EnergyCalibrator::setLinCalib(double x, double y){
  a=x;
  b=y;
}

double EnergyCalibrator::eCalib(double channel){
  return a*channel+b;
}
