/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[58];
    char stringdata0[1545];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 19), // "updateDigitizerInfo"
QT_MOC_LITERAL(2, 31, 0), // ""
QT_MOC_LITERAL(3, 32, 15), // "updateRunStatus"
QT_MOC_LITERAL(4, 48, 13), // "updateDisplay"
QT_MOC_LITERAL(5, 62, 6), // "length"
QT_MOC_LITERAL(6, 69, 7), // "double*"
QT_MOC_LITERAL(7, 77, 5), // "array"
QT_MOC_LITERAL(8, 83, 8), // "startDAQ"
QT_MOC_LITERAL(9, 92, 7), // "stopDAQ"
QT_MOC_LITERAL(10, 100, 25), // "on_plotPushButton_clicked"
QT_MOC_LITERAL(11, 126, 37), // "on_dscfdStartLineEdit_editing..."
QT_MOC_LITERAL(12, 164, 39), // "on_filterLenghtLineEdit_editi..."
QT_MOC_LITERAL(13, 204, 33), // "on_saveSettingsPushButton_cli..."
QT_MOC_LITERAL(14, 238, 32), // "on_rebinLineEdit_editingFinished"
QT_MOC_LITERAL(15, 271, 35), // "on_baseLineLineEdit_editingFi..."
QT_MOC_LITERAL(16, 307, 41), // "on_integralLengthLineEdit_edi..."
QT_MOC_LITERAL(17, 349, 35), // "on_dcfdStopLineEdit_editingFi..."
QT_MOC_LITERAL(18, 385, 39), // "on_dcfdFractionLineEdit_editi..."
QT_MOC_LITERAL(19, 425, 40), // "on_dcfdThresholdLineEdit_edit..."
QT_MOC_LITERAL(20, 466, 36), // "on_dcfdDelayLineEdit_editingF..."
QT_MOC_LITERAL(21, 503, 43), // "on_dcfdFilterLengthLineEdit_e..."
QT_MOC_LITERAL(22, 547, 30), // "on_integralCutCheckBox_toggled"
QT_MOC_LITERAL(23, 578, 7), // "checked"
QT_MOC_LITERAL(24, 586, 41), // "on_integralMinThrLineEdit_edi..."
QT_MOC_LITERAL(25, 628, 34), // "on_baseLineSdCutOnCheckBox_to..."
QT_MOC_LITERAL(26, 663, 46), // "on_baseLineSdThresholdLineEdi..."
QT_MOC_LITERAL(27, 710, 28), // "on_dcfdCutOnCheckBox_toggled"
QT_MOC_LITERAL(28, 739, 32), // "on_zcNumberCutOnCheckBox_toggled"
QT_MOC_LITERAL(29, 772, 35), // "on_pisRangeLineEdit_editingFi..."
QT_MOC_LITERAL(30, 808, 39), // "on_recordLengthLineEdit_editi..."
QT_MOC_LITERAL(31, 848, 33), // "on_adcChannelSpinBox_valueCha..."
QT_MOC_LITERAL(32, 882, 4), // "arg1"
QT_MOC_LITERAL(33, 887, 34), // "on_postTriggerSpinBox_valueCh..."
QT_MOC_LITERAL(34, 922, 18), // "saveSettingsToFile"
QT_MOC_LITERAL(35, 941, 20), // "loadSettingsFromFile"
QT_MOC_LITERAL(36, 962, 33), // "on_loadSettingsPushButton_cli..."
QT_MOC_LITERAL(37, 996, 9), // "refreshUI"
QT_MOC_LITERAL(38, 1006, 22), // "on_resetButton_clicked"
QT_MOC_LITERAL(39, 1029, 27), // "on_psdCutOnCheckBox_toggled"
QT_MOC_LITERAL(40, 1057, 33), // "on_psdMinLineEdit_editingFini..."
QT_MOC_LITERAL(41, 1091, 33), // "on_psdMaxLineEdit_editingFini..."
QT_MOC_LITERAL(42, 1125, 37), // "on_tergetZeroLineEdit_editing..."
QT_MOC_LITERAL(43, 1163, 28), // "on_aLineEdit_editingFinished"
QT_MOC_LITERAL(44, 1192, 28), // "on_bLineEdit_editingFinished"
QT_MOC_LITERAL(45, 1221, 18), // "on_saveDecayCurves"
QT_MOC_LITERAL(46, 1240, 39), // "on_triggerThresholdLineEdit_t..."
QT_MOC_LITERAL(47, 1280, 31), // "on_dcOffsetSpinBox_valueChanged"
QT_MOC_LITERAL(48, 1312, 44), // "on_pulsePolarityComboBox_curr..."
QT_MOC_LITERAL(49, 1357, 5), // "index"
QT_MOC_LITERAL(50, 1363, 46), // "on_triggerPolarityComboBox_cu..."
QT_MOC_LITERAL(51, 1410, 32), // "on_rejectClippedCheckBox_toggled"
QT_MOC_LITERAL(52, 1443, 21), // "on_pushButton_clicked"
QT_MOC_LITERAL(53, 1465, 27), // "on_removePushButton_clicked"
QT_MOC_LITERAL(54, 1493, 28), // "on_psTableWidget_cellChanged"
QT_MOC_LITERAL(55, 1522, 3), // "row"
QT_MOC_LITERAL(56, 1526, 6), // "column"
QT_MOC_LITERAL(57, 1533, 11) // "setLogYaxis"

    },
    "MainWindow\0updateDigitizerInfo\0\0"
    "updateRunStatus\0updateDisplay\0length\0"
    "double*\0array\0startDAQ\0stopDAQ\0"
    "on_plotPushButton_clicked\0"
    "on_dscfdStartLineEdit_editingFinished\0"
    "on_filterLenghtLineEdit_editingFinished\0"
    "on_saveSettingsPushButton_clicked\0"
    "on_rebinLineEdit_editingFinished\0"
    "on_baseLineLineEdit_editingFinished\0"
    "on_integralLengthLineEdit_editingFinished\0"
    "on_dcfdStopLineEdit_editingFinished\0"
    "on_dcfdFractionLineEdit_editingFinished\0"
    "on_dcfdThresholdLineEdit_editingFinished\0"
    "on_dcfdDelayLineEdit_editingFinished\0"
    "on_dcfdFilterLengthLineEdit_editingFinished\0"
    "on_integralCutCheckBox_toggled\0checked\0"
    "on_integralMinThrLineEdit_editingFinished\0"
    "on_baseLineSdCutOnCheckBox_toggled\0"
    "on_baseLineSdThresholdLineEdit_editingFinished\0"
    "on_dcfdCutOnCheckBox_toggled\0"
    "on_zcNumberCutOnCheckBox_toggled\0"
    "on_pisRangeLineEdit_editingFinished\0"
    "on_recordLengthLineEdit_editingFinished\0"
    "on_adcChannelSpinBox_valueChanged\0"
    "arg1\0on_postTriggerSpinBox_valueChanged\0"
    "saveSettingsToFile\0loadSettingsFromFile\0"
    "on_loadSettingsPushButton_clicked\0"
    "refreshUI\0on_resetButton_clicked\0"
    "on_psdCutOnCheckBox_toggled\0"
    "on_psdMinLineEdit_editingFinished\0"
    "on_psdMaxLineEdit_editingFinished\0"
    "on_tergetZeroLineEdit_editingFinished\0"
    "on_aLineEdit_editingFinished\0"
    "on_bLineEdit_editingFinished\0"
    "on_saveDecayCurves\0"
    "on_triggerThresholdLineEdit_textChanged\0"
    "on_dcOffsetSpinBox_valueChanged\0"
    "on_pulsePolarityComboBox_currentIndexChanged\0"
    "index\0on_triggerPolarityComboBox_currentIndexChanged\0"
    "on_rejectClippedCheckBox_toggled\0"
    "on_pushButton_clicked\0on_removePushButton_clicked\0"
    "on_psTableWidget_cellChanged\0row\0"
    "column\0setLogYaxis"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      48,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  254,    2, 0x0a /* Public */,
       3,    0,  255,    2, 0x0a /* Public */,
       4,    2,  256,    2, 0x0a /* Public */,
       8,    0,  261,    2, 0x0a /* Public */,
       9,    0,  262,    2, 0x0a /* Public */,
      10,    0,  263,    2, 0x08 /* Private */,
      11,    0,  264,    2, 0x08 /* Private */,
      12,    0,  265,    2, 0x08 /* Private */,
      13,    0,  266,    2, 0x08 /* Private */,
      14,    0,  267,    2, 0x08 /* Private */,
      15,    0,  268,    2, 0x08 /* Private */,
      16,    0,  269,    2, 0x08 /* Private */,
      17,    0,  270,    2, 0x08 /* Private */,
      18,    0,  271,    2, 0x08 /* Private */,
      19,    0,  272,    2, 0x08 /* Private */,
      20,    0,  273,    2, 0x08 /* Private */,
      21,    0,  274,    2, 0x08 /* Private */,
      22,    1,  275,    2, 0x08 /* Private */,
      24,    0,  278,    2, 0x08 /* Private */,
      25,    1,  279,    2, 0x08 /* Private */,
      26,    0,  282,    2, 0x08 /* Private */,
      27,    1,  283,    2, 0x08 /* Private */,
      28,    1,  286,    2, 0x08 /* Private */,
      29,    0,  289,    2, 0x08 /* Private */,
      30,    0,  290,    2, 0x08 /* Private */,
      31,    1,  291,    2, 0x08 /* Private */,
      33,    1,  294,    2, 0x08 /* Private */,
      34,    0,  297,    2, 0x08 /* Private */,
      35,    0,  298,    2, 0x08 /* Private */,
      36,    0,  299,    2, 0x08 /* Private */,
      37,    0,  300,    2, 0x08 /* Private */,
      38,    0,  301,    2, 0x08 /* Private */,
      39,    1,  302,    2, 0x08 /* Private */,
      40,    0,  305,    2, 0x08 /* Private */,
      41,    0,  306,    2, 0x08 /* Private */,
      42,    0,  307,    2, 0x08 /* Private */,
      43,    0,  308,    2, 0x08 /* Private */,
      44,    0,  309,    2, 0x08 /* Private */,
      45,    0,  310,    2, 0x08 /* Private */,
      46,    1,  311,    2, 0x08 /* Private */,
      47,    1,  314,    2, 0x08 /* Private */,
      48,    1,  317,    2, 0x08 /* Private */,
      50,    1,  320,    2, 0x08 /* Private */,
      51,    1,  323,    2, 0x08 /* Private */,
      52,    0,  326,    2, 0x08 /* Private */,
      53,    0,  327,    2, 0x08 /* Private */,
      54,    2,  328,    2, 0x08 /* Private */,
      57,    0,  333,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 6,    5,    7,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   23,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   23,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   23,
    QMetaType::Void, QMetaType::Bool,   23,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   32,
    QMetaType::Void, QMetaType::Int,   32,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   23,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   32,
    QMetaType::Void, QMetaType::QString,   32,
    QMetaType::Void, QMetaType::Int,   49,
    QMetaType::Void, QMetaType::Int,   49,
    QMetaType::Void, QMetaType::Bool,   23,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   55,   56,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->updateDigitizerInfo(); break;
        case 1: _t->updateRunStatus(); break;
        case 2: _t->updateDisplay((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< double*(*)>(_a[2]))); break;
        case 3: _t->startDAQ(); break;
        case 4: _t->stopDAQ(); break;
        case 5: _t->on_plotPushButton_clicked(); break;
        case 6: _t->on_dscfdStartLineEdit_editingFinished(); break;
        case 7: _t->on_filterLenghtLineEdit_editingFinished(); break;
        case 8: _t->on_saveSettingsPushButton_clicked(); break;
        case 9: _t->on_rebinLineEdit_editingFinished(); break;
        case 10: _t->on_baseLineLineEdit_editingFinished(); break;
        case 11: _t->on_integralLengthLineEdit_editingFinished(); break;
        case 12: _t->on_dcfdStopLineEdit_editingFinished(); break;
        case 13: _t->on_dcfdFractionLineEdit_editingFinished(); break;
        case 14: _t->on_dcfdThresholdLineEdit_editingFinished(); break;
        case 15: _t->on_dcfdDelayLineEdit_editingFinished(); break;
        case 16: _t->on_dcfdFilterLengthLineEdit_editingFinished(); break;
        case 17: _t->on_integralCutCheckBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 18: _t->on_integralMinThrLineEdit_editingFinished(); break;
        case 19: _t->on_baseLineSdCutOnCheckBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 20: _t->on_baseLineSdThresholdLineEdit_editingFinished(); break;
        case 21: _t->on_dcfdCutOnCheckBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 22: _t->on_zcNumberCutOnCheckBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 23: _t->on_pisRangeLineEdit_editingFinished(); break;
        case 24: _t->on_recordLengthLineEdit_editingFinished(); break;
        case 25: _t->on_adcChannelSpinBox_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 26: _t->on_postTriggerSpinBox_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 27: _t->saveSettingsToFile(); break;
        case 28: _t->loadSettingsFromFile(); break;
        case 29: _t->on_loadSettingsPushButton_clicked(); break;
        case 30: _t->refreshUI(); break;
        case 31: _t->on_resetButton_clicked(); break;
        case 32: _t->on_psdCutOnCheckBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 33: _t->on_psdMinLineEdit_editingFinished(); break;
        case 34: _t->on_psdMaxLineEdit_editingFinished(); break;
        case 35: _t->on_tergetZeroLineEdit_editingFinished(); break;
        case 36: _t->on_aLineEdit_editingFinished(); break;
        case 37: _t->on_bLineEdit_editingFinished(); break;
        case 38: _t->on_saveDecayCurves(); break;
        case 39: _t->on_triggerThresholdLineEdit_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 40: _t->on_dcOffsetSpinBox_valueChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 41: _t->on_pulsePolarityComboBox_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 42: _t->on_triggerPolarityComboBox_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 43: _t->on_rejectClippedCheckBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 44: _t->on_pushButton_clicked(); break;
        case 45: _t->on_removePushButton_clicked(); break;
        case 46: _t->on_psTableWidget_cellChanged((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 47: _t->setLogYaxis(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 48)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 48;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 48)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 48;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
