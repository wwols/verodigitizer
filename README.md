**veroDigitizer**

---

## Requirements

Before you can use this tool for runing a measurement you need to install:

1. Qt
2. CERN ROOT
3. CEAN digitizer library

---

## Documentation

See in the documentation of the **veroDigitizer** website for more informations: [http://homepage.tudelft.nl/66k3u/verodigitizer.html](http://homepage.tudelft.nl/66k3u/verodigitizer.html)


---

## Installation
Get the source by cloning it from the repository:


```
git clone git@bitbucket.org:wwols/verodigtizer.git
```
or downloading from "Downloads" section at Bitbucket: [https://bitbucket.org/wwols/verodigitizer/downloads/](https://bitbucket.org/wwols/verodigitizer/downloads/)


Type the following in the root directory:

```
make
```


---

## How to use

Start the program like this:

```
./veroDigitizer config.json
```

where config.json is a JSON configuration file.