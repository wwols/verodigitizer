#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "Digitizer.h"


MainWindow::MainWindow(Json::Value root, udp_server::udp_server* server, char* configFileName, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    versionName= "veroDigitizer 1.8 - ";

    m_root = root;
    m_server = server;
    m_configFileName = configFileName;
    ui->setupUi(this);
    analyzer = new EventAnalyzer();
    digitizer = new Digitizer(analyzer);
    digitizer->setConfiguration(m_root,m_server);

    off = new QState();
    off->assignProperty(ui->startButton, "text", "Start");
    off->setObjectName("off");

    on = new QState();
    on->setObjectName("on");
    on->assignProperty(ui->startButton, "text", "Stop");

    off->addTransition(ui->startButton, SIGNAL(clicked()), on);
    on->addTransition(ui->startButton, SIGNAL(clicked()), off);

    machine.addState(off);
    machine.addState(on);

    machine.setInitialState(off);
    machine.start();

    QObject::connect(on,SIGNAL(entered()),this, SLOT(startDAQ()));
    QObject::connect(off,SIGNAL(entered()),this, SLOT(stopDAQ()));

    statusTimer = new QTimer(this);
    connect(statusTimer, SIGNAL(timeout()), this, SLOT(updateRunStatus()));
    connect(digitizer, SIGNAL(arrayReady(int,double*)),this,SLOT(updateDisplay(int,double*)));
    connect(digitizer, SIGNAL(digitizerInfoReceived()),this,SLOT(updateDigitizerInfo()));
    connect(ui->actionSave_decay_curves, SIGNAL(triggered(bool)),this,SLOT(on_saveDecayCurves()));

    refreshUI();

    series = new QLineSeries();
    series->append(0, 1);
    series->append(1, 2);
    series->append(2, 3);
    series->append(3, 4);
    series->append(4, 5);
    *series << QPointF(6, 1) << QPointF(7, 2) << QPointF(8, 3) << QPointF(9, 4) << QPointF(10, 5);
    chart = new QChart();
    chart->legend()->hide();
    chart->addSeries(series);
    chart->createDefaultAxes();
    chart->setTitle("");
    //chart->setAnimationOptions(QChart::SeriesAnimations);

    //ui->chartview->setRenderHint(QPainter::Antialiasing);
/*
    QValueAxis *axisX = new QValueAxis();
    axisX->setTitleText("Data point");
    axisX->setLabelFormat("%i");
    axisX->setTickCount(series->count());
    chart->addAxis(axisX, Qt::AlignBottom);
    series->attachAxis(axisX);

    QLogValueAxis *axisY = new QLogValueAxis();
    axisY->setTitleText("Values");
    axisY->setLabelFormat("%g");
    axisY->setBase(8.0);
    axisY->setMinorTickCount(-1);
    chart->addAxis(axisY, Qt::AlignLeft);
    series->attachAxis(axisY);
*/
 ui->chartview->setChart(chart);

    //connect(ui->startButton,SIGNAL(clicked()), digitizer, SLOT(startDAQ()));
    //connect(ui->stopButton,SIGNAL(clicked()),digitizer,SLOT(stopDAQ()));
}

std::ifstream::pos_type filesize(const char* filename)
{
    std::ifstream in(filename, std::ifstream::ate | std::ifstream::binary);
    return in.tellg();
}

void MainWindow::refreshUI(){
  //Run controll
  this->setWindowTitle(QString::fromStdString(versionName+m_configFileName));
  //ui->fileNameLabel->setText(m_configFileName);

  //L1
  ui->adcChannelSpinBox->setValue(m_root["daq"]["channelIndex"].asInt());
  ui->triggerThresholdLineEdit->setText(QString::number(m_root["daq"]["triggerThreshold"].asInt()));
  ui->dcOffsetSpinBox->setValue(m_root["daq"]["dcOffset"].asUInt());
  ui->postTriggerSpinBox->setValue(m_root["daq"]["postTriggerSize"].asInt());
  ui->recordLengthLineEdit->setText(QString::number(m_root["daq"]["recordLength"].asInt()));
  ui->pulsePolarityComboBox->setCurrentIndex((int)(0.5+0.5*m_root["daq"]["pulsePolarity"].asInt()));
  ui->triggerPolarityComboBox->setCurrentIndex((int)(0.5+0.5*m_root["daq"]["triggerPolarity"].asInt()));

  //L2
  ui->filterLenghtLineEdit->setText(QString::number(m_root["analysis"]["movingAverageLength"].asInt()));
  ui->rebinLineEdit->setText(QString::number(m_root["analysis"]["rebin"].asInt()));
  ui->lowPassFilterCheckBox->setChecked((bool)m_root["analysis"]["lowPassFilterOn"].asInt());
  ui->baseLineLineEdit->setText(QString::number(m_root["analysis"]["baseLineLength"].asInt()));
  ui->integralLengthLineEdit->setText(QString::number(m_root["analysis"]["integralLength"].asInt()));
  ui->pisRangeLineEdit->setText(QString::number(m_root["energyRange"].asDouble()));
  ui->dscfdStartLineEdit->setText(QString::number(m_root["analysis"]["dcfdSettings"]["start"].asInt()));
  ui->dcfdStopLineEdit->setText(QString::number(m_root["analysis"]["dcfdSettings"]["stop"].asInt()));
  ui->dcfdFractionLineEdit->setText(QString::number(m_root["analysis"]["dcfdSettings"]["fraction"].asDouble()));
  ui->dcfdThresholdLineEdit->setText(QString::number(m_root["analysis"]["dcfdSettings"]["threshold"].asDouble()));
  ui->dcfdDelayLineEdit->setText(QString::number(m_root["analysis"]["dcfdSettings"]["delay"].asInt()));
  ui->dcfdFilterLengthLineEdit->setText(QString::number(m_root["analysis"]["dcfdSettings"]["length"].asInt()));
  ui->integralCutCheckBox->setChecked((bool)m_root["analysis"]["qualityCuts"]["integralCutOn"].asInt());
  ui->integralMinThrLineEdit->setText(QString::number(m_root["analysis"]["qualityCuts"]["integralMinThreshold"].asInt()));
  ui->baseLineSdCutOnCheckBox->setChecked((bool)m_root["analysis"]["qualityCuts"]["baseLineSdCutOn"].asInt());
  ui->baseLineSdThresholdLineEdit->setText(QString::number(m_root["analysis"]["qualityCuts"]["baseLineSdThreshold"].asDouble()));
  ui->dcfdCutOnCheckBox->setChecked((bool)m_root["analysis"]["qualityCuts"]["dcfdCutOn"].asInt());
  ui->zcNumberCutOnCheckBox->setChecked((bool)m_root["analysis"]["qualityCuts"]["zcNumberCutOn"].asInt());
  ui->psdCutOnCheckBox->setChecked((bool)m_root["analysis"]["qualityCuts"]["psdCutOn"].asInt());
  ui->psdMinLineEdit->setText(QString::number(m_root["analysis"]["qualityCuts"]["psdMinThreshold"].asDouble()));
  ui->psdMaxLineEdit->setText(QString::number(m_root["analysis"]["qualityCuts"]["psdMaxThreshold"].asDouble()));
  ui->rejectClippedCheckBox->setChecked((bool)m_root["analysis"]["qualityCuts"]["rejectClipped"].asInt());

  //Analysis
  ui->tergetZeroLineEdit->setText(QString::number(m_root["targetZeroPos"].asInt()));
  ui->aLineEdit->setText(QString::number(m_root["EnergyCalibration"]["a"].asDouble()));
  ui->bLineEdit->setText(QString::number(m_root["EnergyCalibration"]["b"].asDouble()));

  ui->psTableWidget->clear();
  ui->psTableWidget->setHorizontalHeaderLabels(QStringList() << "Particle" << "Bins" << "Imin" <<"Imax");
  int size = m_root["analysis"]["PulseShapes"].size();
  for(int i = 0; i<size; i++){
      ui->psTableWidget->insertRow (i); //ui->psTableWidget->rowCount()
      QTableWidgetItem *particleItem = new QTableWidgetItem(m_root["analysis"]["PulseShapes"][i]["particle"].asString().c_str());
      QTableWidgetItem *nBinsItem = new QTableWidgetItem(m_root["analysis"]["PulseShapes"][i]["bins"].asString().c_str());
      QTableWidgetItem *iMinItem = new QTableWidgetItem(m_root["analysis"]["PulseShapes"][i]["Imin"].asString().c_str());
      QTableWidgetItem *iMaxItem = new QTableWidgetItem(m_root["analysis"]["PulseShapes"][i]["Imax"].asString().c_str());
      ui->psTableWidget->setItem(i,0,particleItem);
      ui->psTableWidget->setItem(i,1,nBinsItem);
      ui->psTableWidget->setItem(i,2,iMinItem);
      ui->psTableWidget->setItem(i,3,iMaxItem);
    }

}

void MainWindow::updateDisplay(int length, double* array){
  displayYdata = array;
  displayYdataLength = length;
  if(ui->autoScaleCheckBox->isChecked()) chart->axisX(series)->setRange(0, displayYdataLength);
  //series->clear();
  QVector<QPointF> points;
  for (unsigned int i = 0; i<displayYdataLength; i++){
      points.append(QPointF(i,displayYdata[i]));
    }
  series->replace(points);
  //setLogYaxis();
}

void MainWindow::updateDigitizerInfo(){
  CAEN_DGTZ_BoardInfo_t info = digitizer->getBoardInfo();
  ui->modelNameLabel->setText(info.ModelName);
  ui->modelLabel->setText(QString::number(info.Model));
  ui->channelsLabel->setText(QString::number(info.Channels));
  ui->formFactorLabel->setText(QString::number(info.FormFactor));
  ui->familyCodeLabel->setText(QString::number(info.FamilyCode));
  ui->rocLabel->setText(info.ROC_FirmwareRel);
  ui->amcLabel->setText(info.AMC_FirmwareRel);
  ui->serialNumberLabel->setText(QString::number(info.SerialNumber));
  ui->pcbLabel->setText(QString::number(info.PCB_Revision));
  ui->adcBitsLabel->setText(QString::number(info.ADC_NBits));
  ui->samLabel->setText(QString::number(info.SAMCorrectionDataLoaded));
  ui->commHandle->setText(QString::number(info.CommHandle));
  ui->licenceLabel->setText(info.License);
}

void MainWindow::updateRunStatus(){
  //ui->l1numberLabel->setText(digitizer->getL1Events());
  ui->l1numberLabel->setText(QString::number(digitizer->getL1Events()));
  ui->l2numberLabel->setText(QString::number(digitizer->getL2Events()));
  ui->elapsedTimeLabel->setText(QString::number(digitizer->getRunningTime()));
  ui->l1RateLabel->setText(QString::number(digitizer->getL1Rate(),'f',1));
  ui->l2RateLabel->setText(QString::number(digitizer->getL2Rate(),'f',1));
  ui->receivedBuffersLabel->setText(QString::number(digitizer->getReceivedBuffersN()));
  ui->outputFileSizeLabel->setText(QString::number(filesize("/data/wwols/Code/veroDigitizer/output/Default.root")/1024/1024));
  //std::cout<<"File size:"<<filesize("/data/wwols/Code/veroDigitizer/output/Default.m_root")/1024/1024<<std::endl;
  //series->append(digitizer->getRunningTime(), digitizer->getL2Rate());
  switch(ui->viewComboBox->currentIndex()){
    case 0:
      digitizer->getRawWaveform();
      //displayYdataLength = digitizer->getRawWaveformLength();
      break;
    case 1:
      digitizer->getDecimatedWaveform();
      //displayYdataLength = digitizer->getDecimatedWaveformLength();
      break;
    case 2:
      digitizer->getDcfdWaveform();
      //displayYdataLength = digitizer->getDcfdWaveformLength();
      break;
    case 3:
      digitizer->getPis();
      //displayYdataLength = digitizer->getPisLength();
      break;
    case 5:
      digitizer->getPulseShape();
      //displayYdataLength = digitizer->getPisLength();
      break;
    }

  //chart->zoomReset();
  //chart->createDefaultAxes();
//chart->axisY(series)->setRange(-52, 800);
//chart->axisX(series)->setRange(-52, digitizer->getRunningTime());
}

void MainWindow::startDAQ(){
  ui->resetButton->setEnabled(false);
  digitizer->setConfiguration(m_root,m_server);
  digitizer->startDAQ();
  series->clear();
  statusTimer->start(1000);
}

void MainWindow::stopDAQ(){
  digitizer->stopDAQ();
  statusTimer->stop();
  ui->resetButton->setEnabled(true);
}




MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_plotPushButton_clicked()
{
    chart->axisX(series)->setRange(ui->xMinLineEdit->text(), ui->xMaxLineEdit->text());
    chart->axisY(series)->setRange(ui->yMinLineEdit->text(), ui->yMaxLineEdit->text());
}

void MainWindow::saveSettingsToFile(){
  QString fileName = QFileDialog::getSaveFileName(this,
         tr("Save Settings"), m_configFileName,
         tr("veroDigitizer Settings (*.json);;All Files (*)"));
  if (fileName.isEmpty())
          return;
      else {
      Json::StreamWriterBuilder builder;
      builder["commentStyle"] = "All";
      builder["indentation"] = "\t";
      std::unique_ptr<Json::StreamWriter> writer(builder.newStreamWriter());
      std::string fname = fileName.toStdString();
      m_configFileName = new char [fname.size()+1];
      strcpy(m_configFileName, fname.c_str() );
      //QByteArray array = fileName.toLocal8Bit();
      //char* buffer = array.data();
      //m_configFileName = array.data();
      std::ofstream settingsFile(m_configFileName);
      writer->write(m_root, &settingsFile);
      settingsFile.close();
         }
}

void MainWindow::loadSettingsFromFile(){
  QString fileName = QFileDialog::getOpenFileName(this,
          tr("Open Settings"), "",
          tr("veroDigitizer Settings (*.json);;All Files (*)"));
  if (fileName.isEmpty())
          return;
      else {
        std::string fname = fileName.toStdString();
        m_configFileName = new char [fname.size()+1];
        strcpy(m_configFileName, fname.c_str() );
        //QByteArray array = fileName.toLocal8Bit();
        //char* buffer = array.data();
        //m_configFileName = array.data();
        std::ifstream file(m_configFileName);
        file >> m_root;
        refreshUI();
      }
}

void MainWindow::on_saveSettingsPushButton_clicked()
{
  saveSettingsToFile();
    /*
    Json::StreamWriterBuilder builder;
    builder["commentStyle"] = "All";
    builder["indentation"] = "\t";
    std::unique_ptr<Json::StreamWriter> writer(builder.newStreamWriter());
    std::ofstream settingsFile(m_configFileName);
    writer->write(m_root, &settingsFile);
    settingsFile.close();
    */
}

void MainWindow::on_filterLenghtLineEdit_editingFinished()
{
  m_root["analysis"]["movingAverageLength"] = ui->filterLenghtLineEdit->text().toInt();
}

void MainWindow::on_rebinLineEdit_editingFinished()
{
    m_root["analysis"]["rebin"] = ui->rebinLineEdit->text().toInt();
}

void MainWindow::on_baseLineLineEdit_editingFinished()
{
    m_root["analysis"]["baseLineLength"] = ui->baseLineLineEdit->text().toInt();
}

void MainWindow::on_integralLengthLineEdit_editingFinished()
{
    m_root["analysis"]["integralLength"] = ui->integralLengthLineEdit->text().toInt();
}

void MainWindow::on_dscfdStartLineEdit_editingFinished()
{
  m_root["analysis"]["dcfdSettings"]["start"] = ui->dscfdStartLineEdit->text().toInt();
}


void MainWindow::on_dcfdStopLineEdit_editingFinished()
{
   m_root["analysis"]["dcfdSettings"]["stop"] = ui->dcfdStopLineEdit->text().toInt();
}

void MainWindow::on_dcfdFractionLineEdit_editingFinished()
{
  m_root["analysis"]["dcfdSettings"]["fraction"] = ui->dcfdFractionLineEdit->text().toDouble();
}

void MainWindow::on_dcfdThresholdLineEdit_editingFinished()
{
  m_root["analysis"]["dcfdSettings"]["threshold"] = ui->dcfdThresholdLineEdit->text().toDouble();
}

void MainWindow::on_dcfdDelayLineEdit_editingFinished()
{
  m_root["analysis"]["dcfdSettings"]["delay"] = ui->dcfdDelayLineEdit->text().toInt();
}

void MainWindow::on_dcfdFilterLengthLineEdit_editingFinished()
{
  m_root["analysis"]["dcfdSettings"]["length"] = ui->dcfdFilterLengthLineEdit->text().toInt();
}

void MainWindow::on_integralCutCheckBox_toggled(bool checked)
{
if(checked) m_root["analysis"]["qualityCuts"]["integralCutOn"] = 1;
else m_root["analysis"]["qualityCuts"]["integralCutOn"] = 0;
}

void MainWindow::on_integralMinThrLineEdit_editingFinished()
{
  m_root["analysis"]["qualityCuts"]["integralMinThreshold"] = ui->integralMinThrLineEdit->text().toInt();
}

void MainWindow::on_baseLineSdCutOnCheckBox_toggled(bool checked)
{
  if(checked) m_root["analysis"]["qualityCuts"]["baseLineSdCutOn"] = 1;
  else m_root["analysis"]["qualityCuts"]["baseLineSdCutOn"] = 0;
}

void MainWindow::on_baseLineSdThresholdLineEdit_editingFinished()
{
m_root["analysis"]["qualityCuts"]["baseLineSdThreshold"] = ui->baseLineSdThresholdLineEdit->text().toDouble();
}

void MainWindow::on_dcfdCutOnCheckBox_toggled(bool checked)
{
  if(checked) m_root["analysis"]["qualityCuts"]["dcfdCutOn"] = 1;
  else m_root["analysis"]["qualityCuts"]["dcfdCutOn"] = 0;
}

void MainWindow::on_zcNumberCutOnCheckBox_toggled(bool checked)
{
  if(checked) m_root["analysis"]["qualityCuts"]["zcNumberCutOn"] = 1;
  else m_root["analysis"]["qualityCuts"]["zcNumberCutOn"] = 0;
}

void MainWindow::on_pisRangeLineEdit_editingFinished()
{
  m_root["energyRange"] = ui->pisRangeLineEdit->text().toDouble();
}

void MainWindow::on_adcChannelSpinBox_valueChanged(int arg1)
{
  m_root["daq"]["channelIndex"] = arg1;
}

void MainWindow::on_recordLengthLineEdit_editingFinished()
{
  m_root["daq"]["recordLength"] = ui->recordLengthLineEdit->text().toInt();
}


/*void MainWindow::on_dcOffsetSpinBox_valueChanged(unsigned int arg1)
{
  m_root["daq"]["dcOffset"] = arg1;
  std::cout<<"Kogut"<<std::endl;
}*/

void MainWindow::on_postTriggerSpinBox_valueChanged(int arg1)
{
  m_root["daq"]["postTriggerSize"] = arg1;
}

void MainWindow::on_loadSettingsPushButton_clicked()
{
  loadSettingsFromFile();
}

void MainWindow::on_resetButton_clicked()
{
    digitizer->reset();
}

void MainWindow::on_psdCutOnCheckBox_toggled(bool checked)
{
  if(checked) m_root["analysis"]["qualityCuts"]["psdCutOn"] = 1;
  else m_root["analysis"]["qualityCuts"]["psdCutOn"] = 0;
}

void MainWindow::on_psdMinLineEdit_editingFinished()
{
  m_root["analysis"]["qualityCuts"]["psdMinThreshold"] = ui->psdMinLineEdit->text().toDouble();
}

void MainWindow::on_psdMaxLineEdit_editingFinished()
{
m_root["analysis"]["qualityCuts"]["psdMaxThreshold"] = ui->psdMaxLineEdit->text().toDouble();
}

void MainWindow::on_tergetZeroLineEdit_editingFinished()
{
   m_root["targetZeroPos"] = ui->tergetZeroLineEdit->text().toInt();
}

void MainWindow::on_aLineEdit_editingFinished()
{
  m_root["EnergyCalibration"]["a"] = ui->aLineEdit->text().toDouble();
}

void MainWindow::on_bLineEdit_editingFinished()
{
  m_root["EnergyCalibration"]["b"] = ui->bLineEdit->text().toDouble();
}

void MainWindow::on_saveDecayCurves(){
  char* decayFileName;
  QString fileName = QFileDialog::getSaveFileName(this,
         tr("Save Settings"), "",
         tr("Decay curves (*.txt);;All Files (*)"));
  if (fileName.isEmpty())
          return;
    else {
      std::string fname = fileName.toStdString();
      decayFileName = new char [fname.size()+1];
      strcpy(decayFileName, fname.c_str() );
    }
  analyzer->saveAscii(decayFileName);
}

void MainWindow::on_triggerThresholdLineEdit_textChanged(const QString &arg1)
{
    m_root["daq"]["triggerThreshold"] = arg1.toInt();
}

void MainWindow::on_dcOffsetSpinBox_valueChanged(const QString &arg1)
{
  m_root["daq"]["dcOffset"] = arg1.toUInt();
}

void MainWindow::on_pulsePolarityComboBox_currentIndexChanged(int index)
{
    if(index==0) m_root["daq"]["pulsePolarity"] = -1; else m_root["daq"]["pulsePolarity"] = 1;
}

void MainWindow::on_triggerPolarityComboBox_currentIndexChanged(int index)
{
    if(index==0) m_root["daq"]["triggerPolarity"] = -1; else m_root["daq"]["triggerPolarity"] = 1;
}

void MainWindow::on_rejectClippedCheckBox_toggled(bool checked)
{
    if(checked) m_root["analysis"]["qualityCuts"]["rejectClipped"] = 1;
    else m_root["analysis"]["qualityCuts"]["rejectClipped"] = 0;
}

void MainWindow::on_pushButton_clicked()
{
  QTableWidgetItem *particleItem = new QTableWidgetItem("0");
  QTableWidgetItem *nBinsItem = new QTableWidgetItem("0");
  QTableWidgetItem *iMinItem = new QTableWidgetItem("0");
  QTableWidgetItem *iMaxItem = new QTableWidgetItem("0");

  int rowCount = ui->psTableWidget->rowCount();
  ui->psTableWidget->insertRow (rowCount);

  ui->psTableWidget->setItem(rowCount,0,particleItem);
  ui->psTableWidget->setItem(rowCount,1,nBinsItem);
  ui->psTableWidget->setItem(rowCount,2,iMinItem);
  ui->psTableWidget->setItem(rowCount,3,iMaxItem);
  //int size = pulseShapeDescriptions_.size();
  //pulseShapeDescriptions_[size]["particle"]=0;
  //pulseShapeDescriptions_[size]["bins"]=0;
  //pulseShapeDescriptions_[size]["Imin"]=0;
  //pulseShapeDescriptions_[size]["Imax"]=10;
  //std::cout<<pulseShapeDescriptions_.size()<<std::endl;
  //m_root["analysis"]["PulseShapes"] = pulseShapeDescriptions_;
}



void MainWindow::on_removePushButton_clicked()
{
  int currentRow = ui->psTableWidget->currentRow();
  ui->psTableWidget->removeRow(currentRow);
  Json::Value got;
  m_root["analysis"]["PulseShapes"].removeIndex(currentRow, &got);
}

void MainWindow::on_psTableWidget_cellChanged(int row, int column)
{
  std::cout<<row<<" "<<column<<std::endl;
  char* valueName;
  switch(column){
    case 0:
      valueName = (char*) "particle";
      break;
    case 1:
      valueName = (char*) "bins";
      break;
    case 2:
      valueName = (char*) "Imin";
      break;
    case 3:
      valueName = (char*) "Imax";
      break;
    }
  m_root["analysis"]["PulseShapes"][row][valueName] =  ui->psTableWidget->item(row, column)->text().toInt();
}

void MainWindow::setLogYaxis(){
  QLogValueAxis *axisY = new QLogValueAxis();
  axisY->setTitleText("Values");
  axisY->setLabelFormat("%g");
  axisY->setBase(8.0);
  axisY->setMinorTickCount(-1);
  chart->addAxis(axisY, Qt::AlignLeft);
  series->attachAxis(axisY);

}
