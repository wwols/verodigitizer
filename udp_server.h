// UDP Server -- receives UDP packets
//(C)2017 W. Wolszczak/TU Delft

#ifndef UDP_SERVER_H
#define UDP_SERVER_H

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <unistd.h> /* for close() for socket */
#include <stdlib.h>


/*
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdexcept>
*/
//int sock;
//struct sockaddr_in sa;
//const int bufferSize = 1024;
//char buffer[bufferSize];
//ssize_t recsize;
//socklen_t fromlen;



namespace udp_server
{
  /*
  class udp_client_server_runtime_error : public std::runtime_error
  {
  public:
  udp_client_server_runtime_error(const char *w) : std::runtime_error(w) {}
  };
  */

  class udp_server
  {
  public:
    udp_server(int port);
    ~udp_server();
    
    int                 get_socket() const;
    int                 get_port() const;
    //    std::string         get_addr() const;
    
    int                 recvfrom(char *msg, size_t max_size);
    //int                 timed_recv(char *msg, size_t max_size, int max_wait_ms);
    
  private:
    int                 f_socket;
    int                 f_port;
    struct sockaddr_in  sa;
    ssize_t             recsize;
    socklen_t           fromlen;

    //    std::string         f_addr;
    //struct addrinfo *   f_addrinfo;
  };
  
} // namespace udp_client_server
#endif

