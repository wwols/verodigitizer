#include "PulseShape.h"
#include <string>
#include <cstdio>
PulseShape::PulseShape(EnergyCalibrator e, int polarity, double* waveform, long int waveformLength,int particleIndex,int nbIntegralBins,double integralMin,double integralMax, double targetZeroPosition):
  e(e), polarity(polarity), waveform(waveform), waveformLength(waveformLength), particleIndex(particleIndex), nbIntegralBins_(nbIntegralBins), integralMin(integralMin),
  integralMax(integralMax), targetZeroPosition(targetZeroPosition){
  avPulseVec  = new double*[nbIntegralBins];
  normalizedPulseVec = new double*[nbIntegralBins];
  std::cout<<"new normalizedPulseVec"<<std::endl;
  for(int i = 0; i<nbIntegralBins; i++){
      avPulseVec[i] = new double[waveformLength];
      normalizedPulseVec[i] = new double[waveformLength];
    }
  avPulseCounter = new int[nbIntegralBins]; //number of events in each integral bin
  for(int i=0; i<nbIntegralBins;i ++) avPulseCounter[i]=0; //reset all counters
}

PulseShape::~PulseShape(){
  for(int i = 0; i<nbIntegralBins_; i++){
      delete avPulseVec[i];
      delete normalizedPulseVec[i];
    }
  delete avPulseVec;
  delete avPulseCounter;
}

void PulseShape::addWaveform(int particle, double baseLine, double integral, double zeroCrossingTime){ //zeroCrossingTime measured from begining of the waveform, not from dcfdStart
  if(integral<integralMin || integral>integralMax) return;  //accept only events witn integral wthin the integral range
  if(particle!=particleIndex) return; //accept only events of the same particle type

  roundedZero = (int)round(zeroCrossingTime);
  nRange = (int) (integral-integralMin)*nbIntegralBins_/(integralMax-integralMin);
  //std::cout<<"particle="<<particle<<" baseline="<<baseLine<<" integral= "<<integral<<" ZC"<<zeroCrossingTime<<" nRange="<<nRange<<std::endl;
  int zeroDiff = abs(targetZeroPosition-roundedZero);//difference between source array zero and target array zero position

  if(roundedZero>targetZeroPosition) {
    startIndex = zeroDiff;
    stopIndex = waveformLength;
  }
  else{
    startIndex=0;
    stopIndex=waveformLength-zeroDiff;
  }

  for(int i=startIndex;i<stopIndex;i++){
    int targetIndex = i+targetZeroPosition-roundedZero;
    if(i<0) std::cout<<"Jest zle!"<<std::endl;
    if(i>waveformLength) std::cout<<"Jest jeszcze gorzej!"<<std::endl;
    avPulseVec[nRange][targetIndex]+=polarity*(waveform[i]-baseLine);
    //yAllignedAv[targetIndex]+=waveform[i];
  }
  avPulseCounter[nRange]++;
  //std::cout<<avPulseCounter[nRange]<<std::endl;
}

double* PulseShape::getPulseShape(int index){  //return avarage pulse shape in units of ADC bits
  if(index<0 || index>nbIntegralBins_) return NULL;
  if(avPulseCounter[index] == 0) return NULL;
  for(int i=0;i<waveformLength;i++)
    normalizedPulseVec[index][i] = avPulseVec[index][i]/avPulseCounter[index];
  return normalizedPulseVec[index];
}

double* PulseShape::getPulseShape(int index, double norm){ //returns avarage pulse shape normalized to norm integral
  if(index<0 || index>nbIntegralBins_) return NULL;
  if(avPulseCounter[index] == 0) return NULL;
  double integral = 0;
  for(int i=0;i<waveformLength;i++)
    integral+=avPulseVec[index][i];
  if(!std::isnan(integral))
    for(int i=0;i<waveformLength;i++)
      normalizedPulseVec[index][i]= avPulseVec[index][i]*norm/integral;
  else std::cout<<"Warning: Pulse avarage norm is NAN! Skipping normalization."<<std::endl;
  return normalizedPulseVec[index];
}

void PulseShape::normalize(int index, char* option){ //returns avarage pulse shape normalized to what is defined as option
  std::cout<<"index "<<index<<" nbIntegralBins_ "<<nbIntegralBins_<<std::endl;
  if(index<0 || index>nbIntegralBins_) return;
  if(avPulseCounter[index] == 0) return;
  int maxIndex = 0;
  std::string opt = option;
  std::size_t found = opt.find("0-1");
  if(found != std::string::npos){
    for(int i=0;i<waveformLength;i++)
      if(avPulseVec[index][i]>avPulseVec[index][maxIndex]) maxIndex=i;
    for(int i=0;i<waveformLength;i++)
      normalizedPulseVec[index][i] = avPulseVec[index][i]/avPulseVec[index][maxIndex];
  }
}

double* PulseShape::getPulseShape(int index, char* option){ //returns avarage pulse shape normalized to what is defined as option
  if(index<0 || index>nbIntegralBins_) return NULL;
  if(avPulseCounter[index] == 0) return NULL;
  normalize(index, option);
  return normalizedPulseVec[index];
}

void PulseShape::resetCounters(){
  for(int i=0; i<nbIntegralBins_; i++) avPulseCounter[i]=0;
}

void PulseShape::clearBuffers(){
  for(int i=0; i<nbIntegralBins_; i++)
    for(int j=0;j<waveformLength;j++)
      avPulseVec[i][j] = 0;
}

void PulseShape::reset(){
  resetCounters();
  clearBuffers();
}

int PulseShape::saveAscii(char* fileName){
  std::ofstream file;
  try{
    file.open(fileName);
    if(file){
        for(int index = 0; index < nbIntegralBins_; index++){
            if(avPulseCounter[index]>0)
              normalize(index,"0-1");
          }
        double rangeWidth = (integralMax-integralMin)/nbIntegralBins_;
        for(int index=0;index<nbIntegralBins_;index++)
          if(avPulseCounter[index]!=0)
            file<<std::fixed <<std::setprecision(1)<<e.eCalib(index*rangeWidth+integralMin)<<"-"<<e.eCalib((index+1)*rangeWidth+integralMin)<<" keV\t";
        file<<std::endl;
        for (int sample = 0; sample < waveformLength; sample++){
            for (int index=0; index<nbIntegralBins_; index++) {
                if(avPulseCounter[index]!=0)
                  file<<std::scientific<<std::setprecision(5)<<normalizedPulseVec[index][sample]<<"\t";;
              }
            file<<std::endl;

          }
        file.close();
      }
  }
  catch(...){
    std::cout<<"Error: unable to save ascii file!"<<std::endl;
  }
  return 0;
}
