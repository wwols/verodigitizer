#ifndef PULSESHAPE_H
#define PULSESHAPE_H
#include <fstream>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <EnergyCalibrator.h>
//stores waveforms in integral-wise sorted arrays

class PulseShape
{
public:
  PulseShape(EnergyCalibrator e, int polarity, double* waveform, long int waveformLength,int particleIndex,int nbIntegralBins,double integralMin,double integralMax, double targetZeroPosition);
  //polarity 1 if positive pulses, -1 when negative
  ~PulseShape();
  void addWaveform(int particle, double baseLine, double integral, double zeroCrossingTime);
  void resetCounters();
  void clearBuffers();
  void reset();
  int getNbIntegralBins() {return nbIntegralBins_;}
  int getCounter(int index) {return avPulseCounter[index];}
  double getBinWidth() {return (integralMax-integralMin)/nbIntegralBins_;}
  double getImin() {return integralMin;}
  double getImax() {return integralMax;}
  double* getPulseShape(int index);
  double* getPulseShape(int index, double norm);
  double* getPulseShape(int index, char* option);
  int getWaveformLength() {return waveformLength;}
  void normalize(int index, char* option);
  int saveAscii(char* fileName);

private:
  int particleIndex;
  int nbIntegralBins_;
  int nRange;
  int roundedZero;
  int startIndex, stopIndex;
  int polarity;
  long int waveformLength;
  double integralMin;
  double integralMax;
  double targetZeroPosition;
  int* avPulseCounter;
  double* waveform;
  double** avPulseVec;
  double** normalizedPulseVec;
  EnergyCalibrator e;
};

#endif // PULSESHAPE_H
