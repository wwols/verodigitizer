#ifndef DIGITIZER_H
#define DIGITIZER_H
#include "CAENDigitizer.h"
#include "keyb.h"
#include <json/json.h>
#include "udp_server.h"
#include "EventAnalyzer.h"
#include <time.h>
#include <QThread>
#include <QMutex>
#include <iterator>
#include <algorithm>

class Digitizer : public QThread {
    Q_OBJECT

public:
  Digitizer(EventAnalyzer *anal, QObject *parent = 0);
 ~Digitizer();
  void setConfiguration(Json::Value troot, udp_server::udp_server* tserver);

 CAEN_DGTZ_ErrorCode configureDigitizer(); //uint32_t* recordLength

 int getL1Events(){
   return totalNumbOfEvents;
 }

 int getL2Events(){
   return count;
 }

 double getL1Rate(){
   return totalNumbOfEvents/runningTime;
 }

 double getL2Rate(){
   return count/runningTime;
 }

 int getRunningTime(){
   return runningTime;
 }

 int getReceivedBuffersN(){
   return b;
 }

void getRawWaveform();

void getDecimatedWaveform();

void getDcfdWaveform();

void getPis();

void getPulseShape();

 int getPisLength(){
   return analyzer->getPisLength();
 }

 int getDecimatedWaveformLength(){
   return analyzer->getDecimatedWaveformLength();
 }

 int getDcfdWaveformLength(){
   return analyzer->getDcfdWaveformLength();
 }

 uint32_t getRawWaveformLength(){
   return recordLength;
 }

 int getPulseShapeLength(){
   return analyzer->getPulseShapeLength();
 }

 CAEN_DGTZ_BoardInfo_t getBoardInfo(){
   return BoardInfo;
 };


 int closeDigitizer();

signals:
 void arrayReady(int length, double* array);
 void digitizerInfoReceived();

public slots:
  void stopDAQ();
  void startDAQ();
  void reset();

protected:
  void run();

 private:

  int handle;
  unsigned int i,b=0, count=0;
  unsigned long long int epoch = 0;

  uint32_t  Measuretime = 0;//Measuretime in seconds RecordLength, RecordLength in channels
  uint32_t  maxNumberOfEvents,
    PostTriggerSize,
    verbose=1,
    liveView = 1, //save histo to file
    liveViewRefreshRate = 3, // live view refresing rate in seconds
    lastLiveViewSaved=0, //time of last histo save
    countRate = 0;
  uint32_t ChannelEnableMask = 0;
  uint32_t recordLength, t0;
  uint32_t size, bsize, numEvents;
  uint32_t totalNumbOfEvents = 0;
  uint32_t lastDigitizerTime;
  uint32_t numbOfClipped = 0;
  uint32_t ADC_NBits;

  Json::Value root;
  udp_server::udp_server* server;

  static const int udpBufferSize = 1024;
  char udpBuffer[udpBufferSize];
  char *buffer = NULL, *evtptr = NULL;
  const char* countRateFileName = "countRate.txt";
  const char* phsFileName = "phs.txt";
  const char* patternFileName = "pattern1.txt";

  double timeTag, runningTime, previousRunningTime;
  struct timespec requestStart, requestEnd;
  double* rawWaveform;
  double* decimatedWaveform;
  double* dcfdWaveform;
  double* pis;
  double* pulseShape;

  //CAEN_DGTZ_ErrorCode ret;
  //CAEN_DGTZ_BoardInfo_t BoardInfo;
  CAEN_DGTZ_EventInfo_t eventInfo;
  CAEN_DGTZ_UINT16_EVENT_t *Evt = NULL;
  CAEN_DGTZ_ErrorCode ret;
  CAEN_DGTZ_BoardInfo_t BoardInfo;

  EventAnalyzer* analyzer;

  QMutex mutex;
  bool m_abort, m_getRawWaveform, m_getDecimatedWaveform, m_getDcfdWaveform, m_getPis, m_getPulseShape;

};


#endif
