#ifndef DCFD_H
#define DCFD_H
#include<vector>

class Dcfd{
 private:
  double * y;
  unsigned int minPosition ;
  unsigned int zeroPosition;
  unsigned int overThreshold;
  unsigned int arrayLength;
  double preciseZero;
  unsigned int L, D;
  double F;
  double * array; //unsinged int
  unsigned int startSample;
  unsigned int stopSample;
  double threshold;

 public:
  Dcfd(unsigned int pL, unsigned int pD, double pF, double * pArray, unsigned int pStartSample, unsigned int pStopSample, double pThreshold);//: L(pL), D(pD),
  //F(pF), array(pArray), startSample(pStartSample),
  //stopSample(pStopSample), threshold(pThreshold){};
  //N - length of the input array, L-length of moving avarage [samples], D-lenght of delay [samples], F-fraction of delay signal [0.-1.], BL-base line, array-input array
  ~Dcfd();
  void processSignal(double bl);
  double getZeroCrossing(void);
  std::vector<double> getZeroCrossingVec(void);
  double* getOutputSignal(void);
};

#endif
