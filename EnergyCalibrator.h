#ifndef ENERGYCALIBRATOR_H
#define ENERGYCALIBRATOR_H

class EnergyCalibrator{
 private:
  double a, b;
  
 public:
  EnergyCalibrator();
  ~EnergyCalibrator();

  void setLinCalib(double x, double y);
  double eCalib(double channel);
};

#endif
