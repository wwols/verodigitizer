#define LINUX
#define CAEN_USE_DIGITIZERS
#define IGNORE_DPP_DEPRECATED
#define MAXNB 1 /* Number of connected boards */
#include <cmath>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include "CAENDigitizer.h"
#include <iostream>
#include "keyb.h"
#include "dcfd.h"
#include <json/json.h>
#include <fstream>
#include <vector>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include "udp_server.h"
#include "EventAnalyzer.h"
//#include "Digitizer.h"
#include <sstream>
#include <string>
#include <stdio.h>
#include <QApplication>
#include <QLabel>
#include <QPushButton>
#include "mainwindow.h"

using namespace std;

const int udpBufferSize = 1024;
const int port = 7654;
char buffer[udpBufferSize];
int state = 0; //0 idle, 1 daq, 2 exit

void helloWorld(){
  cout<<endl;
  cout<<"========================================="<<endl;
  cout<<"     Nuclear Data Acquisition System     "<<endl;
  cout<<"       (C)2015-2018 W. Wolszczak         "<<endl;
  cout<<"      Version: 1.7     2018-02-02        "<<endl;
  cout<<"========================================="<<endl;
  cout<<endl;
}

int main(int sys_argc, char ** sys_argv){
  if(sys_argc==1) {
    cout<<"No arguments. Use: ./TestDigitizer configuration.json"<<endl;
    return -1;
  }

  Json::Value root;
  std::ifstream file(sys_argv[1]);
  file >> root;

  //  Digitizer *digitizer = new Digitizer(root);
  udp_server::udp_server* server = new udp_server::udp_server(port);
 // Digitizer *digitizer = new Digitizer(root,server);

  helloWorld();
  cout<<"To start measurement press 's'."<<endl;
  QApplication app(sys_argc, sys_argv);
  MainWindow mainWindow(root,server, sys_argv[1]);
  mainWindow.show();

  /*
  int c = 0;
  for(;;){
    sleep(1);
    ssize_t recsize;
    recsize = server->recvfrom(buffer,udpBufferSize);

    if(recsize>0){
      printf("recsize: %d\n ", (int)recsize);
      printf("datagram: %.*s\n", (int)recsize, buffer);
      if (strcmp("start",buffer)==0) state = 1; //cout<<"Chuzia na juzia!"<<endl;
      if (strcmp("stop", buffer)==0) state = 0;// cout<<"Prrrr!"<<endl;
      if (strcmp("exit", buffer)==0) state = 2;
      memset(buffer,0,udpBufferSize);
    }

    if(kbhit()){
      c = getch();
      if(c=='s') state = 1; 
      if(c=='q') state = 2;
    }

    if(state == 1) {
      cout<<"Starting data acuqisition!"<<endl;
      digitizer->dataAcquistion();
      cout<<"To start measurement press 's'. To quit press 'q'."<<endl;
      state = 0;
    }

    if(state == 2) {
      cout<<"Exiting."<<endl; return EXIT_SUCCESS;
    }  
  }

  */
  return app.exec();
}
