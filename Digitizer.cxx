#include<iostream>
#include<fstream>
#include "Digitizer.h"

using namespace std;

Digitizer::Digitizer(EventAnalyzer *anal, QObject *parent)
                     : QThread(parent)
{
  analyzer = anal;
  m_abort = false;
  m_getRawWaveform = false;
  m_getDcfdWaveform = false;
  m_getDcfdWaveform = false;
  m_getPis = false;
  m_getPulseShape = false;
  recordLength = 0;
  previousRunningTime = 0;
}

Digitizer::~Digitizer(){
  mutex.lock();
  m_abort = true;
  mutex.unlock();
  wait();
}

void Digitizer::reset(){
  m_abort = false;
  m_getRawWaveform = false;
  m_getDcfdWaveform = false;
  m_getDcfdWaveform = false;
  m_getPis = false;
  recordLength = 0;
  previousRunningTime = 0;
  runningTime = 0;
  b = 0;
  totalNumbOfEvents = 0;
  count = 0;
  analyzer->reset();
}

void Digitizer::getRawWaveform(){
  mutex.lock();
  m_getRawWaveform = true;
  mutex.unlock();
}

void Digitizer::getDecimatedWaveform(){
  mutex.lock();
  m_getDecimatedWaveform = true;
  mutex.unlock();
}

void Digitizer::getDcfdWaveform(){
  mutex.lock();
  m_getDcfdWaveform = true;
  mutex.unlock();
}

void Digitizer::getPis(){
  mutex.lock();
  m_getPis = true;
  mutex.unlock();
}

void Digitizer::getPulseShape(){
  mutex.lock();
  m_getPulseShape = true;
  mutex.unlock();
}

void Digitizer::setConfiguration(Json::Value troot, udp_server::udp_server* tserver){
  root = troot;
  server = tserver;
}

void Digitizer::startDAQ()
{
  m_abort = false;
  start();
}

void Digitizer::stopDAQ()
{
  mutex.lock();
  m_abort = true;
  mutex.unlock();
}

CAEN_DGTZ_ErrorCode Digitizer::configureDigitizer(){ //uint32_t* recordLength
  ChannelEnableMask = 1<<root["daq"]["channelIndex"].asUInt();
  
  /* Once we have the handler to the digitizer, we use it to call the other functions */
  ret = CAEN_DGTZ_GetInfo(handle, &BoardInfo);
  emit digitizerInfoReceived();
  printf("\nConnected to CAEN Digitizer Model %s, recognized as board %d\n", BoardInfo.ModelName, 0);
  printf("\tROC FPGA Release is %s\n", BoardInfo.ROC_FirmwareRel);
  printf("\tAMC FPGA Release is %s\n\n", BoardInfo.AMC_FirmwareRel);
  ADC_NBits = BoardInfo.ADC_NBits;

  cout<<(uint32_t)root["daq"]["recordLength"].asInt()<<endl;
  cout<<(uint32_t)root["daq"]["postTriggerSize"].asUInt()<<endl;
  cout<<(uint32_t)root["daq"]["channelIndex"].asUInt()<<endl;
  cout<<"dcOffset: "<<root["daq"]["dcOffset"].asUInt()<<endl;
  cout<<root["daq"]["triggerThreshold"].asUInt()<<endl;
  cout<<(uint32_t)root["daq"]["maxNumEventsBLT"].asUInt()<<endl;
  CAEN_DGTZ_PulsePolarity_t pulsePolarity;
  CAEN_DGTZ_TriggerPolarity_t triggerPolarity;
  if(root["daq"]["pulsePolarity"].asInt()==-1) pulsePolarity = CAEN_DGTZ_PulsePolarityNegative; else pulsePolarity = CAEN_DGTZ_PulsePolarityPositive;
  if(root["daq"]["triggerPolarity"].asInt()==-1) triggerPolarity = CAEN_DGTZ_TriggerOnFallingEdge; else triggerPolarity = CAEN_DGTZ_TriggerOnRisingEdge;

  ret = CAEN_DGTZ_Reset(handle);                                               // Reset Digitizer
  ret = CAEN_DGTZ_GetInfo(handle,&BoardInfo);                                 // Get Board Info
  ret = CAEN_DGTZ_SetRecordLength(handle,(uint32_t)root["daq"]["recordLength"].asInt()); /// Set the lenght of each waveform (in samples)
  ret = CAEN_DGTZ_GetRecordLength(handle,&recordLength); //exact value of each waveform
  ret = CAEN_DGTZ_SetChannelEnableMask(handle,ChannelEnableMask);                                      // Enable channel "i"
  ret = CAEN_DGTZ_SetPostTriggerSize(handle,(uint32_t)root["daq"]["postTriggerSize"].asUInt());                                // Enable channel
  ret = CAEN_DGTZ_SetChannelDCOffset(handle,(uint32_t)root["daq"]["channelIndex"].asUInt(),(uint16_t)root["daq"]["dcOffset"].asUInt());
  ret = CAEN_DGTZ_SetTriggerPolarity(handle,(uint32_t)root["daq"]["channelIndex"].asUInt(),triggerPolarity); //CAEN_DGTZ_TriggerOnFallingEdge
  ret = CAEN_DGTZ_SetChannelTriggerThreshold(handle,(uint32_t)root["daq"]["channelIndex"].asUInt(),(uint32_t)root["daq"]["triggerThreshold"].asUInt()); // Set selfTrigger thres\hold
  ret = CAEN_DGTZ_SetChannelSelfTrigger(handle,CAEN_DGTZ_TRGMODE_ACQ_ONLY,ChannelEnableMask);  // Set self trigger to be ACQ_ONLY
  //ret = CAEN_DGTZ_SetChannelSelfTrigger(handle,CAEN_DGTZ_TRGMODE_DISABLED,ChannelEnableMask); //wylaczam self tyger tutaj!!!
  ret = CAEN_DGTZ_SetChannelPulsePolarity(handle,(uint32_t)root["daq"]["channelIndex"].asUInt(),pulsePolarity);// CAEN_DGTZ_PulsePolarityNegative Set trigger on channel 0to be ACQ_ONLY
  cout<<"Pulse polarity: "<<pulsePolarity<<endl;
  cout<<"Trigger polarity: "<<triggerPolarity<<endl;

  ret = CAEN_DGTZ_SetSWTriggerMode(handle,CAEN_DGTZ_TRGMODE_ACQ_ONLY); // Set the behaviour when a SW tirgger arrives
  //ret = CAEN_DGTZ_SetExtTriggerInputMode(handle, CAEN_DGTZ_TRGMODE_ACQ_ONLY); //wlaczam external trigger tutaj!
  ret = CAEN_DGTZ_SetMaxNumEventsBLT(handle,(uint32_t)root["daq"]["maxNumEventsBLT"].asUInt());  // Set the max number of events to transfer in a single readout
  ret = CAEN_DGTZ_SetAcquisitionMode(handle,CAEN_DGTZ_SW_CONTROLLED);          // Set the acquisition mode
  rawWaveform = new double[recordLength];
  analyzer->setBitRange(ADC_NBits);
  analyzer->loadSettings(root,recordLength);
  cout<<"decimated waveform length:"<<analyzer->getDecimatedWaveformLength()<<endl;
  decimatedWaveform = new double[analyzer->getDecimatedWaveformLength()];
  dcfdWaveform = new double[analyzer->getDcfdWaveformLength()];
  pis = new double[analyzer->getPisLength()];
  pulseShape = new double[analyzer->getPulseShapeLength()];
  return ret;
}

void Digitizer::run(){
  verbose = root["display"]["verbose"].asInt();
  maxNumberOfEvents = root["daq"]["maxNumberOfEvents"].asInt();
  cout<<"Maximum number of events: "<<maxNumberOfEvents<<endl;
  lastDigitizerTime = 0;

  ret = CAEN_DGTZ_OpenDigitizer(CAEN_DGTZ_USB, 0, 0, 0, &handle);
  if(ret) {cout<<"Can't open digitizer!"<<endl; closeDigitizer(); return ;}
  ret = configureDigitizer(); // &recordLength niekonieczny argument bo to jest private przeciez


  sleep(1);

  uint32_t ptv;
  ret = CAEN_DGTZ_ReadRegister(handle,0x8114,&ptv);
  cout<<"Post Trigger Value "<<ptv<<endl;

  ret = CAEN_DGTZ_MallocReadoutBuffer(handle,&buffer,&size);

  //Start Acquisition
  cout<<"Starting Data Acquisition"<<endl;
  if(runningTime!=0) previousRunningTime = runningTime;
  clock_gettime(CLOCK_MONOTONIC, &requestStart); //was CLOCK_REALTIME
  ret = CAEN_DGTZ_SWStartAcquisition(handle);
  //printf("SW start %d \n",NumberOfEvents);
  sleep(1);

  b=0;
  for(;;){ //infinite loop
      /*
    int c = 0;
    if(kbhit()){
      c = getch();
      if(c=='q') break;
    }
    ssize_t recsize;
    recsize = server->recvfrom(udpBuffer,udpBufferSize);
    if(recsize>0){
      printf("recsize: %d\n ", (int)recsize);
      printf("datagram: %.*s\n", (int)recsize, buffer);
      if (strcmp("stop", udpBuffer)==0) break;// cout<<"Prrrr!"<<endl;

    }
  */

    cout<<"Buffer no="<<b<<"\t"<<flush;
    int failed = 0;
    while (1){
      //if(SaveExternalTemperature) ambientTemperature = str2d(exec("python LakeShoreM331-TempCheck.py"));
      ret = CAEN_DGTZ_ReadData(handle,CAEN_DGTZ_SLAVE_TERMINATED_READOUT_MBLT,buffer,&bsize); /* Read the buffer from the digitizer */
      if (bsize != 0) break;
      //if (failed > 10) CAEN_DGTZ_ClearData(handle);//goto error;
      if (m_abort) {
          cout<<"Abort signal! Stoping DAQ!"<<endl;
          closeDigitizer();
          return ;
      }
      if (failed==50) cout<<"Read data fail..."<<flush;
      if (failed>50) {
        ret = CAEN_DGTZ_SendSWtrigger(handle);
        cout<<"Sending software trigger!"<<endl;
        //ret = CAEN_DGTZ_WriteRegister(handle,0x8108,0);
        //cout<<"."<<flush;
      }
      if (failed > 2000) {
        uint32_t eventsStored;
        ret = CAEN_DGTZ_ReadRegister(handle,0x812c,&eventsStored);
        uint32_t acqStatus;
        ret = CAEN_DGTZ_ReadRegister(handle,0x8104,&acqStatus);
        cout<<"Events stored in the Output Buffer: "<<eventsStored<<endl;
        cout<<"Over-temperature [CH3:CH0]: "<< ((acqStatus & 1<<20)>>20) <<endl;
        cout<<"Channel shutdown status: "<< ((acqStatus & 1<<19)>>19) <<endl;
        cout<<"Board ready: "<< ((acqStatus & 1<<8)>>8) <<endl;
        cout<<"Event full: "<< ((acqStatus & 1<<4)>>4) <<endl;
        cout<<"Acq status: "<< ((acqStatus & 1<<2)>>2) <<endl;

	closeDigitizer();
	return ;

      }
      failed++;
      sleep(1);
    }
    cout<<endl;
    clock_gettime(CLOCK_MONOTONIC, &requestEnd); //was CLOCK_REALTIME
    runningTime = previousRunningTime + (requestEnd.tv_sec - requestStart.tv_sec)  + (requestEnd.tv_nsec - requestStart.tv_nsec)/1.e9;
    //check(ret == CAEN_DGTZ_Success, "Errors during data read out.\n");
    ret = CAEN_DGTZ_GetNumEvents(handle,buffer,bsize,&numEvents);
    cout<<"numEvents : "<<numEvents<<"\t"<<flush;
    for (i=0;i<numEvents;i++) {
      /* Get the Infos and pointer to the event */
      ret = CAEN_DGTZ_GetEventInfo(handle,buffer,bsize,i,&eventInfo,&evtptr);
      if (i==0) t0=eventInfo.TriggerTimeTag;
      timeTag = (double)(eventInfo.TriggerTimeTag*8e-9);
      if(eventInfo.TriggerTimeTag < lastDigitizerTime) epoch++;
      lastDigitizerTime = eventInfo.TriggerTimeTag;
      double digitizerTime =(double) (epoch*17.179869176 + eventInfo.TriggerTimeTag*8E-9);

      if(verbose) printf("\tevent info : %d %d %d %d %d %f %u %Lu %f\n",eventInfo.EventSize,eventInfo.BoardId,eventInfo.Pattern,eventInfo.ChannelMask,eventInfo.EventCounter,timeTag,eventInfo.TriggerTimeTag, epoch, digitizerTime);
      ret = CAEN_DGTZ_DecodeEvent(handle,evtptr,(void**)&Evt);

      if(Evt){
        analyzer->analyze(Evt);
        int res = analyzer->fill();
        if(res == 0) {

            if(m_getRawWaveform){
                mutex.lock();
                if(analyzer->getRawWaveform() != NULL)
                  std::copy(analyzer->getRawWaveform(),analyzer->getRawWaveform()+recordLength,rawWaveform);
                m_getRawWaveform = false;
                mutex.unlock();
                emit arrayReady(getRawWaveformLength(),rawWaveform);
              }
            if(m_getDecimatedWaveform){
                mutex.lock();
                if(analyzer->getDecimatedWaveform() != NULL)
                  std::copy(analyzer->getDecimatedWaveform(),analyzer->getDecimatedWaveform()+analyzer->getDecimatedWaveformLength(),decimatedWaveform);
                m_getDecimatedWaveform = false;
                mutex.unlock();
                emit arrayReady(getDecimatedWaveformLength(),decimatedWaveform);
              }
            if(m_getDcfdWaveform){
                mutex.lock();
                if(analyzer->getDcfdWaveform() != NULL)
                  std::copy(analyzer->getDcfdWaveform(),analyzer->getDcfdWaveform()+analyzer->getDcfdWaveformLength(),dcfdWaveform);
                m_getDcfdWaveform = false;
                mutex.unlock();
                emit arrayReady(getDcfdWaveformLength(),dcfdWaveform);
              }
            if(m_getPis){
                mutex.lock();
                if(analyzer->getPis() != NULL)
                  std::copy(analyzer->getPis(),analyzer->getPis()+analyzer->getPisLength(),pis);
                m_getPis = false;
                mutex.unlock();
                emit arrayReady(getPisLength(),pis);
            }
            if(m_getPulseShape){
                mutex.lock();
                if(analyzer->getPulseShape(0,0) != NULL)
                  std::copy(analyzer->getPulseShape(0,0),analyzer->getPulseShape(0,0)+analyzer->getPulseShapeLength(),pulseShape);
                m_getPulseShape = false;
                mutex.unlock();
                emit arrayReady(analyzer->getPulseShapeLength(),pulseShape);
            }

          count++;
          //analyzer->allignPulse();
        }
      }
      ret = CAEN_DGTZ_FreeEvent(handle,(void**)&Evt);
      //if(eventInfo.EventCounter > NumberOfEvents-2) break;
      //    b++;
    }

    totalNumbOfEvents +=numEvents;
    cout<<"Base line: "<<analyzer->getBaseLine()<<endl;
    //cout<<" TrigThrshld : "<<TriggerThreshold<<"\tBL : "<<baseLine<<flush; //16383 for 14bit
    cout<<"\tPC time : "<<runningTime<<"\ttotalNumbOfEvents : "<<totalNumbOfEvents<<endl;

    if (((unsigned int)count > maxNumberOfEvents-1) || ((runningTime>Measuretime) && (Measuretime>0))) {
      cout<<"Maximum number of events or maximum time has been reached. Exiting..."<<endl;
      closeDigitizer();
      return ;
    }

    if (m_abort) {
        cout<<"Abort signal! Stoping DAQ!"<<endl;
        closeDigitizer();
        return ;
    }

    b++;
  }
  delete rawWaveform;
  delete decimatedWaveform;

  closeDigitizer();
  return ;
}

int Digitizer::closeDigitizer(){
  ret = CAEN_DGTZ_SWStopAcquisition(handle);
  cout<<"Acquisition stopped!"<<endl<<endl;
  if(totalNumbOfEvents>0){
      cout<<"Total number of triggerd events: "<<totalNumbOfEvents<<endl;
      cout<<"Events saved: "<<count<<"("<<100*count/totalNumbOfEvents<<"%)"<<endl; //dzielenie przez zero!
      cout<<"Clipped events: "<<numbOfClipped<<" ("<<100*numbOfClipped/totalNumbOfEvents<<"%)"<<endl;
      cout<<"Maximum number of events to save: "<<maxNumberOfEvents<<endl;
      cout<<"Runing time: "<<runningTime<<" s"<<endl;
      cout<<"Average saving rate: "<<count/runningTime<<" Hz"<<endl;
      cout<<"Average triggering rate: "<<totalNumbOfEvents/runningTime<<" Hz"<<endl;

      //analyzer->normalizePulses();
      //analyzer.filterPulses();
      analyzer->createGraphs();
      analyzer->save();
      analyzer->saveAscii();

    }
  else cout<<"No events triggered."<<endl;

  // Free the buffers and close the digitizer
  cout<<"Error: "<<ret<<endl;
  ret = CAEN_DGTZ_FreeReadoutBuffer(&buffer);
  ret = CAEN_DGTZ_CloseDigitizer(handle);
  //printf("Press 'Enter' key to exit\n");
  //c = getchar();
  return ret;
}


